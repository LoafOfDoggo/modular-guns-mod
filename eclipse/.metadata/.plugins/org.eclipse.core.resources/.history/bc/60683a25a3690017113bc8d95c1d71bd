package net.decimation.mod.common.block.props;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.Random;
import net.decimation.mod.common.CommonPlayerData;
import net.decimation.mod.common.ItemManager;
import net.decimation.mod.common.entity.blockentities.props.EntityWaterfountain;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockWaterfountain extends BlockContainer {
   public BlockWaterfountain(String tileName) {
      super(Material.iron);
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.2F, 1.0F);
      this.setHardness(1.0F);
      this.setHarvestLevel("pickaxe", 4);
      this.setBlockName(tileName);
   }

   @SideOnly(Side.CLIENT)
   public void randomDisplayTick(World world, int x, int y, int z, Random random) {
      if(random.nextInt(15) == 0) {
         world.playSound((double)((float)x + 0.5F), (double)((float)y + 0.5F), (double)((float)z + 0.5F), "deci:block.waterpump.drip", 0.5F, 1.0F, false);
      }

   }

   public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float subX, float subY, float subZ) {
      if(world.isRemote) {
         return true;
      } else {
         ItemStack itemstack = player.inventory.getCurrentItem();
         if(itemstack == null) {
            CommonPlayerData.get(player).giveWater(1);
            world.playSoundEffect((double)x, (double)y, (double)z, "random.drink", 0.5F, 1.0F);
            return true;
         } else {
            ItemStack itemstack1;
            if(itemstack.getItem() == Items.glass_bottle) {
               itemstack1 = new ItemStack(ItemManager.itemBottledWater, 1, 0);
               player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GRAY + "You used a Waterpump to fill an empty Bottle"));
               world.playSoundEffect((double)x, (double)y, (double)z, "deci:block.waterpump.use", 1.0F, 1.0F);
               if(!player.inventory.addItemStackToInventory(itemstack1)) {
                  world.spawnEntityInWorld(new EntityItem(world, (double)x + 0.5D, (double)y + 1.5D, (double)z + 0.5D, itemstack1));
               } else if(player instanceof EntityPlayerMP) {
                  ((EntityPlayerMP)player).sendContainerToPlayer(player.inventoryContainer);
               }

               --itemstack.stackSize;
               if(itemstack.stackSize <= 0) {
                  player.inventory.setInventorySlotContents(player.inventory.currentItem, (ItemStack)null);
               }
            } else if(itemstack.getItem() == ItemManager.itemEmptyCan) {
               itemstack1 = new ItemStack(ItemManager.itemCannedWater, 1, 0);
               player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GRAY + "You used a Waterfountain to fill an empty Can"));
               world.playSoundEffect((double)x, (double)y, (double)z, "deci:block.waterpump.use", 1.0F, 1.0F);
               if(!player.inventory.addItemStackToInventory(itemstack1)) {
                  world.spawnEntityInWorld(new EntityItem(world, (double)x + 0.5D, (double)y + 1.5D, (double)z + 0.5D, itemstack1));
               } else if(player instanceof EntityPlayerMP) {
                  ((EntityPlayerMP)player).sendContainerToPlayer(player.inventoryContainer);
               }

               --itemstack.stackSize;
               if(itemstack.stackSize <= 0) {
                  player.inventory.setInventorySlotContents(player.inventory.currentItem, (ItemStack)null);
               }
            } else {
               if(itemstack.getItem() != ItemManager.itemRagDirty) {
                  CommonPlayerData.get(player).giveWater(1);
                  world.playSoundEffect((double)x, (double)y, (double)z, "random.drink", 0.5F, 1.0F);
                  return true;
               }

               itemstack1 = new ItemStack(ItemManager.itemRag, 1, 0);
               player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GRAY + "You used a Waterfountain to clean a Dirty Rag"));
               world.playSoundEffect((double)x, (double)y, (double)z, "deci:block.waterpump.use", 1.0F, 1.0F);
               if(!player.inventory.addItemStackToInventory(itemstack1)) {
                  world.spawnEntityInWorld(new EntityItem(world, (double)x + 0.5D, (double)y + 1.5D, (double)z + 0.5D, itemstack1));
               } else if(player instanceof EntityPlayerMP) {
                  ((EntityPlayerMP)player).sendContainerToPlayer(player.inventoryContainer);
               }

               --itemstack.stackSize;
               if(itemstack.stackSize <= 0) {
                  player.inventory.setInventorySlotContents(player.inventory.currentItem, (ItemStack)null);
               }
            }

            return false;
         }
      }
   }

   public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack) {
      int l = MathHelper.floor_double((double)(par5EntityLivingBase.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
      if(l == 0) {
         par1World.setBlockMetadataWithNotify(par2, par3, par4, 5, 2);
      }

      if(l == 1) {
         par1World.setBlockMetadataWithNotify(par2, par3, par4, 2, 2);
      }

      if(l == 2) {
         par1World.setBlockMetadataWithNotify(par2, par3, par4, 3, 2);
      }

      if(l == 3) {
         par1World.setBlockMetadataWithNotify(par2, par3, par4, 4, 2);
      }

   }

   public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
      int meta = world.getBlockMetadata(x, y, z);
      if(meta == 2) {
         this.setBlockBounds(0.0F, 0.0F, 0.05F, 0.7F, 1.2F, 0.95F);
      }

      if(meta == 5) {
         this.setBlockBounds(0.05F, 0.0F, 0.3F, 0.95F, 1.2F, 1.0F);
      }

      if(meta == 3) {
         this.setBlockBounds(0.05F, 0.0F, 0.0F, 0.95F, 1.2F, 0.7F);
      }

      if(meta == 4) {
         this.setBlockBounds(0.3F, 0.0F, 0.05F, 1.0F, 1.2F, 0.95F);
      }

   }

   public TileEntity createNewTileEntity(World world, int var1) {
      return new EntityWaterfountain();
   }

   public int getRenderType() {
      return -1;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
   }

   public void onBlockActivated() {
   }
}
