package net.decimation.mod.server;

import cpw.mods.fml.common.FMLCommonHandler;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import net.decimation.mod.Decimation;
import net.decimation.mod.server.ServerVariables;
import net.decimation.mod.utilities.net.Message_DeleteRadiationZones;
import net.decimation.mod.utilities.net.Message_GetRadiationZones;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class RadiationZoneManager {
   public static void init() throws IOException {
      getZonesFromConfig();
   }

   public static void getZonesFromConfig() throws IOException {
      ArrayList stringArray = new ArrayList();
      File file = new File("decimation_radiationzones.properties");
      if(!file.exists()) {
         file.createNewFile();
      }

      if(file.exists()) {
         BufferedReader in = new BufferedReader(new FileReader("decimation_radiationzones.properties"));

         String str;
         while((str = in.readLine()) != null) {
            stringArray.add(str);
         }

         ServerVariables.radiationZoneCoordinates = stringArray;
      }

   }

   public static boolean isPlayerInZone(EntityPlayer givenPlayer, MinecraftServer relatedServer) {
      for(int i = 0; i < ServerVariables.radiationZoneCoordinates.size(); ++i) {
         String toSplit = (String)ServerVariables.radiationZoneCoordinates.get(i);
         toSplit.replaceAll("\\s+", "");
         String[] parts = toSplit.split("=");
         String set1 = parts[0];
         String set2 = parts[1];
         String[] toSplit_1 = set1.split("/");
         String[] toSplit_2 = set2.split("/");
         int s1x = Integer.parseInt(toSplit_1[0]);
         int s1y = Integer.parseInt(toSplit_1[1]);
         int s1z = Integer.parseInt(toSplit_1[2]);
         int s2x = Integer.parseInt(toSplit_2[0]);
         int s2y = Integer.parseInt(toSplit_2[1]);
         int s2z = Integer.parseInt(toSplit_2[2]);
         if(isBetween(s1x, s2x, (int)givenPlayer.posX) && isBetween(s1y, s2y, (int)givenPlayer.posY) && isBetween(s1z, s2z, (int)givenPlayer.posZ)) {
            return true;
         }
      }

      return false;
   }

   public static boolean isPlayerNearZone(EntityPlayer givenPlayer, MinecraftServer relatedServer) {
      for(int i = 0; i < ServerVariables.radiationZoneCoordinates.size(); ++i) {
         String toSplit = (String)ServerVariables.radiationZoneCoordinates.get(i);
         toSplit.replaceAll("\\s+", "");
         String[] parts = toSplit.split("=");
         String set1 = parts[0];
         String set2 = parts[1];
         String[] toSplit_1 = set1.split("/");
         String[] toSplit_2 = set2.split("/");
         int s1x = Integer.parseInt(toSplit_1[0]);
         int s1y = Integer.parseInt(toSplit_1[1]);
         int s1z = Integer.parseInt(toSplit_1[2]);
         int s2x = Integer.parseInt(toSplit_2[0]);
         int s2y = Integer.parseInt(toSplit_2[1]);
         int s2z = Integer.parseInt(toSplit_2[2]);
         if(isNear(s1x, s2x, (int)givenPlayer.posX) && isNear(s1y, s2y, (int)givenPlayer.posY) && isNear(s1z, s2z, (int)givenPlayer.posZ)) {
            return true;
         }
      }

      return false;
   }

   public static void deleteZone(EntityPlayer givenPlayer) throws IOException {
      for(int i = 0; i < ServerVariables.radiationZoneCoordinates.size(); ++i) {
         String toSplit = (String)ServerVariables.radiationZoneCoordinates.get(i);
         toSplit.replaceAll("\\s+", "");
         String[] parts = toSplit.split("=");
         String set1 = parts[0];
         String set2 = parts[1];
         String[] toSplit_1 = set1.split("/");
         String[] toSplit_2 = set2.split("/");
         int s1x = Integer.parseInt(toSplit_1[0]);
         int s1y = Integer.parseInt(toSplit_1[1]);
         int s1z = Integer.parseInt(toSplit_1[2]);
         int s2x = Integer.parseInt(toSplit_2[0]);
         int s2y = Integer.parseInt(toSplit_2[1]);
         int s2z = Integer.parseInt(toSplit_2[2]);
         if(isBetween(s1x, s2x, (int)givenPlayer.posX) && isBetween(s1y, s2y, (int)givenPlayer.posY) && isBetween(s1z, s2z, (int)givenPlayer.posZ)) {
            try {
               removeCoordinateFromConfig(toSplit);
               givenPlayer.addChatComponentMessage(new ChatComponentText("Removing radiation zone " + EnumChatFormatting.GREEN + toSplit));
            } catch (IOException var15) {
               var15.printStackTrace();
            }
         }
      }

      getZonesFromConfig();
      clearCoordinatesGlobal();
      refreshCoordinatesGlobal();
   }

   public static void refreshCoordinatesGlobal() {
      clearCoordinatesGlobal();

      for(int i = 0; i < ServerVariables.radiationZoneCoordinates.size(); ++i) {
         Decimation.mainNetworkChannel.sendToAll(new Message_GetRadiationZones((String)ServerVariables.radiationZoneCoordinates.get(i)));
      }

   }

   public static void addZone(int givenX_1, int givenY_1, int givenZ_1, int givenX_2, int givenY_2, int givenZ_2, EntityPlayer givenPlayer) throws IOException {
      if(FMLCommonHandler.instance().getSide().isServer()) {
         File file = new File("decimation_radiationzones.properties");
         if(file.exists()) {
            BufferedWriter output = new BufferedWriter(new FileWriter(file.getName(), true));
            BufferedReader br = new BufferedReader(new FileReader(file.getName()));
            if(br.readLine() == null) {
               output.append(givenX_1 + "/" + givenY_1 + "/" + givenZ_1 + "=" + givenX_2 + "/" + givenY_2 + "/" + givenZ_2);
            } else {
               output.append("\n" + givenX_1 + "/" + givenY_1 + "/" + givenZ_1 + "=" + givenX_2 + "/" + givenY_2 + "/" + givenZ_2);
            }

            output.close();
            getZonesFromConfig();
            refreshCoordinatesGlobal();
            givenPlayer.addChatMessage(new ChatComponentText("Added new Radiation Zone!"));
         } else {
            givenPlayer.addChatComponentMessage(new ChatComponentText("Radiation Zone config file not found! Please create!"));
         }
      } else {
         givenPlayer.addChatComponentMessage(new ChatComponentText("Can\'t create a Radiation Zone in Singleplayer!"));
      }

   }

   public static void clearCoordinatesGlobal() {
      Decimation.mainNetworkChannel.sendToAll(new Message_DeleteRadiationZones());
   }

   public static void removeCoordinateFromConfig(String givenCoordinates) throws IOException {
      File inputFile = new File("decimation_radiationzones.properties");
      File tempFile = new File("decimation_radiationzones_temp.properties");
      BufferedReader reader = new BufferedReader(new FileReader(inputFile));
      BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
      String lineToRemove = givenCoordinates;
      short BUFFER_SIZE = 1000;

      String currentLine;
      while((currentLine = reader.readLine()) != null) {
         String trimmedLine = currentLine.trim();
         if(!trimmedLine.equals(lineToRemove)) {
            if(reader.readLine() != null) {
               reader.mark(BUFFER_SIZE);
               writer.write(currentLine + System.getProperty("line.separator"));
               reader.reset();
            } else {
               writer.write(currentLine);
            }
         }
      }

      writer.close();
      reader.close();
      inputFile.delete();
      tempFile.renameTo(inputFile);
   }

   public static boolean isBetween(int a, int b, int c) {
      return b >= a?c >= a && c <= b:c >= b && c <= a;
   }

   public static boolean isNear(int a, int b, int c) {
      return b >= a?c >= a - 10 && c <= b + 10:c >= b - 10 && c <= a + 10;
   }
}
