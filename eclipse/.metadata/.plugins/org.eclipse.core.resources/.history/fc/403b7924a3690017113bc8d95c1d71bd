package net.decimation.mod.utilities.net;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.decimation.mod.Decimation;
import net.decimation.mod.common.CommonPlayerData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class Message_PlayerData implements IMessage {
   public int humanityLevel;
   public int swordSkill;
   public int swordLevel;
   public int gunSkill;
   public int gunLevel;
   public int shieldSkill;
   public int shieldLevel;
   public int medicalSkill;
   public int medicalLevel;
   public boolean condition_brokenleg;
   public boolean condition_infected;
   public boolean condition_bleeding;
   public boolean condition_irradiated;
   public int water;
   public int blood;
   public int bottleCaps;
   public int stance;
   public float light;
   public int coordinate_x_1;
   public int coordinate_y_1;
   public int coordinate_z_1;
   public int coordinate_x_2;
   public int coordinate_y_2;
   public int coordinate_z_2;
   public int pickupTimer;
   public EntityPlayer player = null;
   public int gesture;
   public ItemStack backpack;
   public ItemStack vest;
   public ItemStack mask;

   public Message_PlayerData() {
   }

   public Message_PlayerData(CommonPlayerData data) {
      this.humanityLevel = data.getHumanityLevel();
      this.swordSkill = data.getSwordSkill();
      this.swordLevel = data.getSwordLevel();
      this.gunSkill = data.getGunSkill();
      this.gunLevel = data.getGunLevel();
      this.shieldSkill = data.getShieldSkill();
      this.shieldLevel = data.getShieldLevel();
      this.medicalSkill = data.getMedicalSkill();
      this.medicalLevel = data.getMedicalLevel();
      this.condition_brokenleg = data.isLegBroken();
      this.condition_infected = data.isInfected();
      this.condition_bleeding = data.isBleeding();
      this.condition_irradiated = data.isIrradiated();
      this.water = data.getWater();
      this.blood = data.getBlood();
      this.bottleCaps = data.getBottleCaps();
      this.stance = data.getStance();
      this.light = data.light;
      this.coordinate_x_1 = data.giveCoordX_1();
      this.coordinate_y_1 = data.giveCoordY_1();
      this.coordinate_z_1 = data.giveCoordZ_1();
      this.coordinate_x_2 = data.giveCoordX_2();
      this.coordinate_y_2 = data.giveCoordY_2();
      this.coordinate_z_2 = data.giveCoordZ_2();
      this.pickupTimer = data.getPickupTimer();
      this.gesture = data.getGesture();
      this.backpack = data.backpack;
      this.vest = data.vest;
      this.mask = data.mask;
   }

   public void fromBytes(ByteBuf in) {
      this.humanityLevel = in.readUnsignedByte();
      this.swordSkill = in.readUnsignedByte();
      this.swordLevel = in.readUnsignedByte();
      this.gunSkill = in.readUnsignedByte();
      this.gunLevel = in.readUnsignedByte();
      this.shieldSkill = in.readUnsignedByte();
      this.shieldLevel = in.readUnsignedByte();
      this.medicalSkill = in.readUnsignedByte();
      this.medicalLevel = in.readUnsignedByte();
      this.condition_brokenleg = in.readBoolean();
      this.condition_infected = in.readBoolean();
      this.condition_bleeding = in.readBoolean();
      this.condition_irradiated = in.readBoolean();
      this.water = in.readUnsignedByte();
      this.blood = in.readUnsignedByte();
      this.bottleCaps = in.readInt();
      this.stance = in.readInt();
      this.light = in.readFloat();
      this.coordinate_x_1 = in.readInt();
      this.coordinate_y_1 = in.readInt();
      this.coordinate_z_1 = in.readInt();
      this.coordinate_x_2 = in.readInt();
      this.coordinate_y_2 = in.readInt();
      this.coordinate_z_2 = in.readInt();
      this.pickupTimer = in.readInt();
      this.gesture = in.readInt();
      this.backpack = ByteBufUtils.readItemStack(in);
      this.vest = ByteBufUtils.readItemStack(in);
      this.mask = ByteBufUtils.readItemStack(in);
   }

   public void toBytes(ByteBuf out) {
      out.writeByte(this.humanityLevel);
      out.writeByte(this.swordSkill);
      out.writeByte(this.swordLevel);
      out.writeByte(this.gunSkill);
      out.writeByte(this.gunLevel);
      out.writeByte(this.shieldSkill);
      out.writeByte(this.shieldLevel);
      out.writeByte(this.medicalSkill);
      out.writeByte(this.medicalLevel);
      out.writeBoolean(this.condition_brokenleg);
      out.writeBoolean(this.condition_infected);
      out.writeBoolean(this.condition_bleeding);
      out.writeBoolean(this.condition_irradiated);
      out.writeByte(this.water);
      out.writeByte(this.blood);
      out.writeInt(this.bottleCaps);
      out.writeInt(this.stance);
      out.writeFloat(this.light);
      out.writeInt(this.coordinate_x_1);
      out.writeInt(this.coordinate_y_1);
      out.writeInt(this.coordinate_z_1);
      out.writeInt(this.coordinate_x_2);
      out.writeInt(this.coordinate_y_2);
      out.writeInt(this.coordinate_z_2);
      out.writeInt(this.pickupTimer);
      out.writeInt(this.gesture);
      ByteBufUtils.writeItemStack(out, this.backpack);
      ByteBufUtils.writeItemStack(out, this.vest);
      ByteBufUtils.writeItemStack(out, this.mask);
   }

   public static class Handler implements IMessageHandler {
      public IMessage onMessage(Message_PlayerData message, MessageContext ctx) {
         Decimation.proxy.handlePlayerDataMessage(message);
         return null;
      }
   }
}
