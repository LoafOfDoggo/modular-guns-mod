package net.decimation.mod.server.clans;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;
import net.decimation.mod.Decimation;
import net.decimation.mod.server.clans.EnumClanRank;
import net.decimation.mod.utilities.net.Message_ClearClanMembers;
import net.decimation.mod.utilities.net.Message_GetClanMembers;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.UsernameCache;

public class ClanManager {
   static String clansDir = "clans/";
   static String clanInvites = "clansinvites/";

   public static void initializeClans() throws IOException {
      File playerList = getPlayerConfig();
      File clanInvitesDirectory = new File(clanInvites);
      File clansDirectory = new File(clansDir);
      if(!clanInvitesDirectory.exists()) {
         clanInvitesDirectory.mkdir();
      }

      if(!clansDirectory.exists()) {
         clansDirectory.mkdir();
      }

      if(!playerList.exists()) {
         System.out.println("Player Data File " + playerList.getName() + " doesn\'t exist! Creating!");
         playerList.createNewFile();
      }

   }

   public static void invitePlayerToClan(String givenPlayerID, String givenClanName) throws IOException {
      File invitesFile = new File(clanInvites + givenClanName + ".clan");
      if(!invitesFile.exists()) {
         invitesFile.createNewFile();
         System.out.println("Invites File " + invitesFile.getName() + " doesn\'t exist! Creating!");
      }

      if(invitesFile.exists()) {
         Scanner scanner = new Scanner(invitesFile);
         ArrayList list = new ArrayList();

         while(scanner.hasNextLine()) {
            list.add(scanner.nextLine());
         }

         if(!list.contains(givenPlayerID)) {
            BufferedWriter output = new BufferedWriter(new FileWriter(invitesFile, true));
            BufferedReader br = new BufferedReader(new FileReader(invitesFile));
            if(br.readLine() == null) {
               output.append(givenPlayerID);
            } else {
               output.append("\n" + givenPlayerID);
            }

            output.close();
         }
      }

   }

   public static void removePlayerInvite(String givenPlayerID, String givenClanName) throws IOException {
      File invitesFile = new File(clanInvites + givenClanName + ".clan");
      File tempInvitesFile = new File(clanInvites + givenClanName + "Temp.clan");
      BufferedReader reader = new BufferedReader(new FileReader(invitesFile));
      BufferedWriter writer = new BufferedWriter(new FileWriter(tempInvitesFile));
      short BUFFER_SIZE = 1000;

      String currentLine;
      while((currentLine = reader.readLine()) != null) {
         String trimmedLine = currentLine.trim();
         if(!trimmedLine.equals(givenPlayerID)) {
            if(reader.readLine() != null) {
               reader.mark(BUFFER_SIZE);
               writer.write(currentLine + System.getProperty("line.separator"));
               reader.reset();
            } else {
               writer.write(currentLine);
            }
         }
      }

      writer.close();
      reader.close();
      invitesFile.delete();
      tempInvitesFile.renameTo(invitesFile);
   }

   public static boolean doesPlayerHaveInvite(String givenPlayerID, String givenClanName) throws IOException {
      File invitesFile = new File(clanInvites + givenClanName + ".clan");
      if(!invitesFile.exists()) {
         invitesFile.createNewFile();
      }

      if(invitesFile.exists()) {
         BufferedReader br = new BufferedReader(new FileReader(invitesFile));
         Throwable var4 = null;

         boolean var6;
         try {
            String line;
            do {
               if((line = br.readLine()) == null) {
                  return false;
               }
            } while(!line.contains(givenPlayerID));

            var6 = true;
         } catch (Throwable var16) {
            var4 = var16;
            throw var16;
         } finally {
            if(br != null) {
               if(var4 != null) {
                  try {
                     br.close();
                  } catch (Throwable var15) {
                     var4.addSuppressed(var15);
                  }
               } else {
                  br.close();
               }
            }

         }

         return var6;
      } else {
         return false;
      }
   }

   public static boolean doesClanExist(String clanName) throws IOException {
      File clanFile = new File(clansDir + clanName.toLowerCase() + ".clan");
      return clanFile.exists();
   }

   public static boolean isPlayerInClan(String givenPlayerID) throws IOException {
      File playerConfig = getPlayerConfig();
      if(playerConfig.exists()) {
         BufferedReader br = new BufferedReader(new FileReader(playerConfig));
         Throwable var3 = null;

         try {
            String line;
            try {
               while((line = br.readLine()) != null) {
                  if(line.startsWith("=" + givenPlayerID)) {
                     boolean var5 = true;
                     return var5;
                  }
               }
            } catch (Throwable var15) {
               var3 = var15;
               throw var15;
            }
         } finally {
            if(br != null) {
               if(var3 != null) {
                  try {
                     br.close();
                  } catch (Throwable var14) {
                     var3.addSuppressed(var14);
                  }
               } else {
                  br.close();
               }
            }

         }
      }

      return false;
   }

   public static void createClan(String clanName) throws IOException {
      File clanFile = getClanConfig(clanName);
      if(!clanFile.exists()) {
         clanFile.createNewFile();
      }

   }

   public static ArrayList getPlayersInClan(String clanName) throws IOException {
      File clanFile = getClanConfig(clanName);
      ArrayList players = new ArrayList();
      if(!clanFile.exists()) {
         return null;
      } else {
         BufferedReader br = new BufferedReader(new FileReader(clanFile));
         Throwable var4 = null;

         ArrayList line1;
         try {
            String line;
            while((line = br.readLine()) != null) {
               if(line.startsWith("=")) {
                  String formatThis = line.replaceAll("=", "");
                  String[] formattedLine = formatThis.split(":");
                  players.add(formattedLine[0]);
               }
            }

            line1 = players;
         } catch (Throwable var15) {
            var4 = var15;
            throw var15;
         } finally {
            if(br != null) {
               if(var4 != null) {
                  try {
                     br.close();
                  } catch (Throwable var14) {
                     var4.addSuppressed(var14);
                  }
               } else {
                  br.close();
               }
            }

         }

         return line1;
      }
   }

   public static ArrayList getPlayersInClanAsEntities(String clanName) throws IOException {
      File clanFile = getClanConfig(clanName);
      ArrayList players = new ArrayList();
      if(!clanFile.exists()) {
         return null;
      } else {
         BufferedReader br = new BufferedReader(new FileReader(clanFile));
         Throwable var4 = null;

         ArrayList line1;
         try {
            String line;
            while((line = br.readLine()) != null) {
               if(line.startsWith("=")) {
                  String formatThis = line.replaceAll("=", "");
                  String[] formattedLine = formatThis.split(":");
                  MinecraftServer server = MinecraftServer.getServer();
                  if(server.getEntityWorld().getPlayerEntityByName(UsernameCache.getLastKnownUsername(UUID.fromString(formattedLine[0]))) != null) {
                     players.add(server.getEntityWorld().getPlayerEntityByName(UsernameCache.getLastKnownUsername(UUID.fromString(formattedLine[0]))));
                  }
               }
            }

            line1 = players;
         } catch (Throwable var16) {
            var4 = var16;
            throw var16;
         } finally {
            if(br != null) {
               if(var4 != null) {
                  try {
                     br.close();
                  } catch (Throwable var15) {
                     var4.addSuppressed(var15);
                  }
               } else {
                  br.close();
               }
            }

         }

         return line1;
      }
   }

   public static ArrayList getPlayersInClanAsUsername(String clanName) throws IOException {
      File clanFile = getClanConfig(clanName);
      ArrayList players = new ArrayList();
      if(!clanFile.exists()) {
         return null;
      } else {
         BufferedReader br = new BufferedReader(new FileReader(clanFile));
         Throwable var4 = null;

         ArrayList line1;
         try {
            String line;
            while((line = br.readLine()) != null) {
               if(line.startsWith("=")) {
                  String formatThis = line.replaceAll("=", "");
                  String[] formattedLine = formatThis.split(":");
                  String playerUsername = UsernameCache.getLastKnownUsername(UUID.fromString(formattedLine[0]));
                  players.add(playerUsername);
               }
            }

            line1 = players;
         } catch (Throwable var16) {
            var4 = var16;
            throw var16;
         } finally {
            if(br != null) {
               if(var4 != null) {
                  try {
                     br.close();
                  } catch (Throwable var15) {
                     var4.addSuppressed(var15);
                  }
               } else {
                  br.close();
               }
            }

         }

         return line1;
      }
   }

   public static void removePlayerFromClan(String givenPlayerID) throws IOException {
      File clanConfig = getClanConfig(getPlayerClan(givenPlayerID));
      File tempClanConfig = new File(clansDir + getPlayerClan(givenPlayerID) + "_Temp.clan");
      BufferedReader reader = new BufferedReader(new FileReader(clanConfig));
      BufferedWriter writer = new BufferedWriter(new FileWriter(tempClanConfig));
      short BUFFER_SIZE = 1000;

      String currentLine;
      while((currentLine = reader.readLine()) != null) {
         String playerConfig = currentLine.trim();
         String[] tempPlayerConfig = playerConfig.split(":");
         if(!tempPlayerConfig[0].contains(givenPlayerID)) {
            if(reader.readLine() != null) {
               reader.mark(BUFFER_SIZE);
               writer.write(currentLine + System.getProperty("line.separator"));
               reader.reset();
            } else {
               writer.write(currentLine);
            }
         }
      }

      writer.close();
      reader.close();
      clanConfig.delete();
      tempClanConfig.renameTo(clanConfig);
      File playerConfig1 = getPlayerConfig();
      File tempPlayerConfig1 = new File("playerDataTemp.clan");
      BufferedReader reader2 = new BufferedReader(new FileReader(playerConfig1));
      BufferedWriter writer2 = new BufferedWriter(new FileWriter(tempPlayerConfig1));
      short BUFFER_SIZE_2 = 1000;

      String currentLine2;
      while((currentLine2 = reader2.readLine()) != null) {
         String trimmedLine = currentLine2.trim();
         String[] formattedLine = trimmedLine.split(":");
         if(!formattedLine[0].contains(givenPlayerID)) {
            if(reader2.readLine() != null) {
               reader2.mark(BUFFER_SIZE_2);
               writer2.write(currentLine2 + System.getProperty("line.separator"));
               reader2.reset();
            } else {
               writer2.write(currentLine2);
            }
         }
      }

      writer2.close();
      reader2.close();
      playerConfig1.delete();
      tempPlayerConfig1.renameTo(playerConfig1);
   }

   public static void deleteClan(String clanName) throws IOException {
      File clanFile = new File(clansDir + clanName.toLowerCase() + ".clan");
      if(clanFile.exists()) {
         clanFile.delete();
      }

      ArrayList clanMembers = getPlayersInClanAsEntities(clanName);

      for(int playerConfig = 0; playerConfig < clanMembers.size(); ++playerConfig) {
         removePlayerFromClan(((EntityPlayer)clanMembers.get(playerConfig)).getGameProfile().getId().toString());
      }

      File var11 = getPlayerConfig();
      File tempPlayerConfig = new File("playerDataTemp.clan");
      BufferedReader reader = new BufferedReader(new FileReader(var11));
      BufferedWriter writer = new BufferedWriter(new FileWriter(tempPlayerConfig));
      short BUFFER_SIZE = 1000;

      String currentLine;
      while((currentLine = reader.readLine()) != null) {
         String trimmedLine = currentLine.trim();
         String[] formattedLine = trimmedLine.split(":");
         if(!formattedLine[1].equals(clanName)) {
            if(reader.readLine() != null) {
               reader.mark(BUFFER_SIZE);
               writer.write(currentLine + System.getProperty("line.separator"));
               reader.reset();
            } else {
               writer.write(currentLine);
            }
         }
      }

      writer.close();
      reader.close();
      var11.delete();
      tempPlayerConfig.renameTo(var11);
   }

   public static File getClanConfig(String clanName) throws IOException {
      File clanFile = new File(clansDir + clanName + ".clan");
      if(!clanFile.exists()) {
         clanFile.createNewFile();
      }

      return clanFile.exists()?clanFile:null;
   }

   public static File getPlayerConfig() throws IOException {
      File playerConfig = new File("playerData.clan");
      if(!playerConfig.exists()) {
         System.out.println("Player config file not found! Creating!");
         playerConfig.createNewFile();
      }

      return playerConfig.exists()?playerConfig:null;
   }

   public static void addPlayerToClan(String playerID, String clanName, EnumClanRank givenRank) throws IOException {
      File clanFile = getClanConfig(clanName);
      if(clanFile.exists()) {
         BufferedWriter output = new BufferedWriter(new FileWriter(clanFile, true));
         BufferedReader br = new BufferedReader(new FileReader(clanFile));
         if(br.readLine() == null) {
            output.append("=" + playerID + ":" + givenRank);
         } else {
            output.append("\n=" + playerID + ":" + givenRank);
         }

         output.close();
      }

   }

   public static EnumClanRank getPlayerRank(String givenPlayerID) throws IOException {
      File clanConfig = getClanConfig(getPlayerClan(givenPlayerID));
      if(clanConfig.exists()) {
         BufferedReader br = new BufferedReader(new FileReader(clanConfig));
         Throwable var3 = null;

         try {
            String playerRank;
            EnumClanRank var7;
            do {
               String line;
               do {
                  if((line = br.readLine()) == null) {
                     return null;
                  }
               } while(!line.startsWith("=" + givenPlayerID));

               String[] splitLine = line.split(":");
               playerRank = splitLine[1];
               if(playerRank.equals("recruit")) {
                  var7 = EnumClanRank.recruit;
                  return var7;
               }

               if(playerRank.equals("officer")) {
                  var7 = EnumClanRank.officer;
                  return var7;
               }
            } while(!playerRank.equals("leader"));

            var7 = EnumClanRank.leader;
            return var7;
         } catch (Throwable var19) {
            var3 = var19;
            throw var19;
         } finally {
            if(br != null) {
               if(var3 != null) {
                  try {
                     br.close();
                  } catch (Throwable var18) {
                     var3.addSuppressed(var18);
                  }
               } else {
                  br.close();
               }
            }

         }
      } else {
         return null;
      }
   }

   public static void addPlayerToClanData(String playerID, String clanName) throws IOException {
      File playerConfig = getPlayerConfig();
      if(playerConfig.exists()) {
         BufferedWriter output = new BufferedWriter(new FileWriter(playerConfig.getName(), true));
         BufferedReader br = new BufferedReader(new FileReader(playerConfig.getName()));
         if(br.readLine() == null) {
            output.append("=" + playerID + ":" + clanName);
         } else {
            output.append("\n=" + playerID + ":" + clanName);
         }

         output.close();
         br.close();
      }

   }

   public static String getPlayerClan(String givenPlayerID) throws IOException {
      File playerConfig = getPlayerConfig();
      if(playerConfig.exists()) {
         BufferedReader br = new BufferedReader(new FileReader(playerConfig));
         Throwable var3 = null;

         try {
            String line;
            try {
               while((line = br.readLine()) != null) {
                  if(line.startsWith("=" + givenPlayerID)) {
                     String[] splitLine = line.split(":");
                     String var6 = splitLine[1];
                     return var6;
                  }
               }
            } catch (Throwable var16) {
               var3 = var16;
               throw var16;
            }
         } finally {
            if(br != null) {
               if(var3 != null) {
                  try {
                     br.close();
                  } catch (Throwable var15) {
                     var3.addSuppressed(var15);
                  }
               } else {
                  br.close();
               }
            }

         }
      }

      return null;
   }

   public static void refreshClans(String playerID) throws IOException {
      MinecraftServer minecraftServer = MinecraftServer.getServer();
      if(minecraftServer.getEntityWorld().func_152378_a(UUID.fromString(playerID)) != null) {
         EntityPlayer thePlayer = minecraftServer.getEntityWorld().func_152378_a(UUID.fromString(playerID));
         ArrayList clanPlayers = getPlayersInClanAsUsername(getPlayerClan(playerID));
         Decimation.mainNetworkChannel.sendToAll(new Message_ClearClanMembers(thePlayer.getEntityId()));

         for(int i = 0; i < clanPlayers.size(); ++i) {
            Decimation.mainNetworkChannel.sendToAll(new Message_GetClanMembers(thePlayer.getEntityId(), (String)clanPlayers.get(i)));
         }
      }

   }
}
