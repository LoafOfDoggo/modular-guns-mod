package com.loaf.modulargun.render;

import org.lwjgl.opengl.GL11;

import com.loaf.modulargun.enums.EnumFPHoldType;
import com.loaf.modulargun.weapons.ItemAmmo;
import com.loaf.modulargun.weapons.ItemGun;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.client.IItemRenderer;

public class RenderGun implements IItemRenderer
{
	private ItemRenderer renderer;
	private ModelBiped modelBiped;
	private ItemAmmo ammo;
	private float animx, animy, animz;
	private int steps = 25;
	private float targetx, targety, targetz;
	
	public RenderGun()
	{
		renderer = new ItemRenderer(Minecraft.getMinecraft());
		modelBiped = new ModelBiped();
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) 
	{
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) 
	{
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) 
	{
		AbstractClientPlayer player = Minecraft.getMinecraft().thePlayer;
		ItemGun gun = (ItemGun)item.getItem();
		
		
		
        if(gun.isReloading) 
        {
        	this.targetz = 45.0F;
        	GL11.glRotatef(animz, 0.0F, 0.0F, 1.0F);
        }
        else if(!gun.isReloading) 
    	{
    		this.targetz = 0.0F;
    		GL11.glRotatef(animz, 0.0F, 0.0F, 1.0F);
    	}
		
		if(type == ItemRenderType.EQUIPPED_FIRST_PERSON) 
		{
			GL11.glPushMatrix();
			
			if((data[1] != null) && ((data[1] instanceof EntityPlayer))) 
			{
				
		        float scale = 2F;
		        GL11.glScalef(scale, scale, scale);
		        	
		        GL11.glTranslatef(-0.0F, -0.35F, -0.3F);
		        GL11.glRotatef(26.0F, 0.0F, 0.0F, 1.0F);
		        GL11.glRotatef(-6.75F, 0.0F, 1.0F, 0.0F);
		    }
			
			IIcon icon = item.getItem().getIcon(item, 0);
			renderer.renderItemIn2D(Tessellator.instance, icon.getMaxU(), icon.getMinV(), icon.getMinU(), icon.getMaxV(), 32, 32, 0.0625F);
			
	        GL11.glPopMatrix();
	        
	        if(gun.holdTypeFP == EnumFPHoldType.ASSAULTRIFLE) 
	        {
		        GL11.glPushMatrix();
		        
		        float scale = 1.0F;
		        GL11.glScalef(scale, scale, scale);
		        
		        GL11.glTranslatef(0.2F, -0.45F, -0.55F);
		        GL11.glRotatef(-45.0F, 0.0F, 0.0F, 1.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedRightArm.render(0.0625F);
				
				GL11.glPopMatrix();
				
				GL11.glPushMatrix();
				
				GL11.glScalef(1.5F, scale, scale);
				
				GL11.glTranslatef(-0.05F, 0.475F, -1.0F);
				GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(40.0F, 1.0F, 0.0F, 0.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedLeftArm.render(0.0625F);
				
				GL11.glPopMatrix();
	        }
	        
	        if(gun.holdTypeFP == EnumFPHoldType.CARBINE) 
	        {
		        GL11.glPushMatrix();
		        
		        float scale = 1.0F;
		        GL11.glScalef(scale, scale, scale);
		        
		        GL11.glTranslatef(0.2F, -0.45F, -0.55F);
		        GL11.glRotatef(-45.0F, 0.0F, 0.0F, 1.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedRightArm.render(0.0625F);
				
				GL11.glPopMatrix();
				
				GL11.glPushMatrix();
				
				GL11.glScalef(1.5F, scale, scale);
				
				GL11.glTranslatef(-0.025F, 0.45F, -1.0F);
				GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(40.0F, 1.0F, 0.0F, 0.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedLeftArm.render(0.0625F);
				
				GL11.glPopMatrix();
	        }
	        
	        if(gun.holdTypeFP == EnumFPHoldType.PISTOL) 
	        {
	        	
		        GL11.glPushMatrix();
		        
		        float scale2 = 1.0F;
		        GL11.glScalef(scale2, scale2, scale2);
		        
		        GL11.glTranslatef(0.2F, -0.45F, -0.55F);
		        GL11.glRotatef(-45.0F, 0.0F, 0.0F, 1.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedRightArm.render(0.0625F);
				
				GL11.glPopMatrix();
				
				GL11.glPushMatrix();
				
				GL11.glScalef(1.5F, scale2, scale2);
				
				GL11.glTranslatef(0.1F, 0.45F, -1.0F);
				GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(40.0F, 1.0F, 0.0F, 0.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedLeftArm.render(0.0625F);
				
				GL11.glPopMatrix();
	        }
	        
	        if(gun.holdTypeFP == EnumFPHoldType.PDW) 
	        {
		        GL11.glPushMatrix();
		        
		        float scale = 1.0F;
		        GL11.glScalef(scale, scale, scale);
		        
		        GL11.glTranslatef(0.2F, -0.45F, -0.55F);
		        GL11.glRotatef(-45.0F, 0.0F, 0.0F, 1.0F);
		        
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedRightArm.render(0.0625F);
				
				GL11.glPopMatrix();
				
				GL11.glPushMatrix();
				
				GL11.glScalef(1.5F, scale, scale);
				
				GL11.glTranslatef(-0.025F, 0.45F, -1.0F);
				GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(40.0F, 1.0F, 0.0F, 0.0F);				
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				modelBiped.bipedLeftArm.render(0.0625F);
				
				GL11.glPopMatrix();
	        }
	        
		}
		
		setupAnimation();
		
		if (type == ItemRenderType.ENTITY) 
		{
			GL11.glPushMatrix();
			
			GL11.glScalef(1.5F, 1.5F, 1.5F);
			
			GL11.glTranslatef(-0.5F, 0.0F, 0.0175F);
			
			IIcon icon = item.getItem().getIcon(item, 0);
			renderer.renderItemIn2D(Tessellator.instance, icon.getMaxU(), icon.getMinV(), icon.getMinU(), icon.getMaxV(), 32, 32, 0.0625F);
			
			GL11.glPopMatrix();
		}
		
		if (type == ItemRenderType.INVENTORY) 
		{
			GL11.glPushMatrix();
			
			float scale = 16F;
			GL11.glScalef(scale, scale, scale);
			
			GL11.glTranslatef(0.85F, 1.0F, 0.0F);
			GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(35.0F, 0.0F, 1.0F, 0.0F);
			
			IIcon icon = item.getItem().getIcon(item, 0);
			renderer.renderItemIn2D(Tessellator.instance, icon.getMaxU(), icon.getMinV(), icon.getMinU(), icon.getMaxV(), 32, 32, 0.0625F);
			
			GL11.glPopMatrix();
		}
	}
	
	public void setupAnimation() 
	{
		float diffX, diffY, diffZ;
		diffX = Math.abs(animx - targetx);
		diffY = Math.abs(animy - targety);
		diffZ = Math.abs(animz - targetz);
		
		if(animx < targetx) 
		{
			animx += diffX / steps;
		}
		
		if(animx > targetx) 
		{
			animx -= diffX / steps;
		}
		
		if(animy < targety) 
		{
			animy += diffY / steps;
		}
		
		if(animy > targety) 
		{
			animy -= diffY / steps;
		}
		
		if(animz < targetz) 
		{
			animz += diffZ / steps;
		}
		
		if(animz > targetz) 
		{
			animz -= diffZ / steps;
		}
	}
}
