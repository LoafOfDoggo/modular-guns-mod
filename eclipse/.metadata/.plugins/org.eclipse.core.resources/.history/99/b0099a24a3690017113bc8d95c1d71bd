package net.decimation.mod.common.physics.corpses;

import java.lang.reflect.Field;
import net.decimation.mod.common.physics.RagdollNode;
import net.decimation.mod.common.physics.RagdollSkeleton;
import net.decimation.mod.common.physics.corpses.BipedCorpse;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class MobCorpse {
   public static MobCorpse[] mobCorpses = new MobCorpse[]{new BipedCorpse()};
   public Class modelClass;

   public MobCorpse(Class argModel) {
      this.modelClass = argModel;
   }

   public static RagdollSkeleton getDefaultSkeleton(Entity argEntity, ModelBase argModel) {
      float s = 0.0625F;
      RagdollNode nodeNeck = new RagdollNode(1.0F, 0.0F, 0.0F + 24.0F * s, 0.0F);
      RagdollNode nodeRightShoulder = new RagdollNode(1.0F, 0.0F + 5.0F * s, 0.0F + 24.0F * s, 0.0F);
      RagdollNode nodeLeftShoulder = new RagdollNode(1.0F, 0.0F - 5.0F * s, 0.0F + 24.0F * s, 0.0F);
      RagdollNode nodePivot = new RagdollNode(1.0F, 0.0F, 0.0F + 12.0F * s, 0.0F);
      RagdollNode nodeRightHip = new RagdollNode(1.0F, 0.0F + 1.9F * s, 0.0F + 12.0F * s, 0.0F);
      RagdollNode nodeLeftHip = new RagdollNode(1.0F, 0.0F - 1.9F * s, 0.0F + 12.0F * s, 0.0F);
      RagdollNode nodeRightLeg = new RagdollNode(1.0F, 0.0F + 1.9F * s, 0.0F, -0.01F);
      RagdollNode nodeLeftLeg = new RagdollNode(1.0F, 0.0F - 1.9F * s, 0.0F, 0.01F);
      RagdollSkeleton skeleton = new RagdollSkeleton();
      nodeRightLeg.attachTo(nodeRightHip, 12.0F * s, 1.0F);
      nodeLeftLeg.attachTo(nodeLeftHip, 12.0F * s, 1.0F);
      nodeRightHip.attachTo(nodePivot, 1.9F * s, 1.0F).setVisible(false);
      nodeLeftHip.attachTo(nodePivot, 1.9F * s, 1.0F).setVisible(false);
      nodeNeck.attachTo(nodePivot, 12.0F * s, 1.0F);
      nodeRightShoulder.attachTo(nodeNeck, 5.0F * s, 1.0F).setVisible(false);
      nodeLeftShoulder.attachTo(nodeNeck, 5.0F * s, 1.0F).setVisible(false);
      nodeRightHip.attachTo(nodeLeftHip, 3.8F * s, 1.0F).setVisible(false);
      nodeNeck.attachTo(nodeRightHip, (float)Math.sqrt(147.61000061035156D) * s, 1.0F).setVisible(false);
      nodeNeck.attachTo(nodeLeftHip, (float)Math.sqrt(147.61000061035156D) * s, 1.0F).setVisible(false);
      nodeRightShoulder.attachTo(nodeLeftShoulder, 10.0F * s, 1.0F).setVisible(false);
      nodeRightShoulder.attachTo(nodeRightHip, 12.0F * s, 1.0F).setVisible(false);
      nodeLeftShoulder.attachTo(nodeLeftHip, 12.0F * s, 1.0F).setVisible(false);
      skeleton.nodeList.add(nodeNeck);
      skeleton.nodeList.add(nodeRightShoulder);
      skeleton.nodeList.add(nodeLeftShoulder);
      skeleton.nodeList.add(nodePivot);
      skeleton.nodeList.add(nodeRightHip);
      skeleton.nodeList.add(nodeLeftHip);
      skeleton.nodeList.add(nodeRightLeg);
      skeleton.nodeList.add(nodeLeftLeg);
      return skeleton;
   }

   public static RagdollSkeleton getSkeletonForModel(Entity argEntity, ModelBase argModel) {
      for(int i = 0; i < mobCorpses.length; ++i) {
         if(mobCorpses[i].modelClass.isInstance(argModel)) {
            return mobCorpses[i].getSkeleton(argEntity, argModel);
         }
      }

      return getDefaultSkeleton(argEntity, argModel);
   }

   public static boolean isEntityRagdolled(EntityLivingBase argEntity) {
      if(argEntity != null && (RenderManager.instance.getEntityRenderObject(argEntity) instanceof RenderLiving || RenderManager.instance.getEntityRenderObject(argEntity) instanceof RenderPlayer)) {
         Field modelField = null;
         ModelBase model = null;

         try {
            modelField = RendererLivingEntity.class.getDeclaredField("mainModel");
         } catch (NoSuchFieldException var10) {
            try {
               modelField = RendererLivingEntity.class.getDeclaredField("field_77045_g");
            } catch (NoSuchFieldException var8) {
               var8.printStackTrace();
            } catch (SecurityException var9) {
               var9.printStackTrace();
            }
         } catch (SecurityException var11) {
            var11.printStackTrace();
         }

         if(modelField != null) {
            try {
               modelField.setAccessible(true);

               try {
                  model = (ModelBase)modelField.get(RenderManager.instance.getEntityRenderObject(argEntity));
               } catch (IllegalAccessException var5) {
                  var5.printStackTrace();
               } catch (IllegalArgumentException var6) {
                  var6.printStackTrace();
               }
            } catch (SecurityException var7) {
               var7.printStackTrace();
            }
         }

         for(int i = 0; i < mobCorpses.length; ++i) {
            if(mobCorpses[i].modelClass.isInstance(model)) {
               return true;
            }
         }
      }

      return false;
   }

   public RagdollSkeleton getSkeleton(Entity argEntity, ModelBase argModel) {
      return getDefaultSkeleton(argEntity, argModel);
   }
}
