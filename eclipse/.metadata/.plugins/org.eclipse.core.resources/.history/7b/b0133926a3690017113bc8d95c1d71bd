package net.decimation.mod.client.gui.api.player_factory;

import com.mojang.authlib.GameProfile;
import java.util.UUID;
import net.decimation.mod.client.ClientProxy;
import net.decimation.mod.client.gui.api.player_factory.FakePlayer;
import net.decimation.mod.client.gui.api.player_factory.FakeWorld;
import net.decimation.mod.utilities.InventoryPersistence;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerPlayer;
import net.minecraft.world.World;

public class FakePlayerFactory {
   private final UUID uuid;
   private final String username;
   private World world;
   private FakePlayer player;

   public FakePlayerFactory(String username, String uuid) {
      this(username, UUID.fromString(uuid));
   }

   public FakePlayerFactory(GameProfile profile) {
      this(profile.getName(), profile.getId());
      this.player.inventoryContainer = new ContainerPlayer(this.player.inventory, false, this.player);
   }

   public FakePlayerFactory(String username, UUID uuid) {
      this.username = username;
      this.uuid = uuid;
      InventoryPersistence.injectPersistedInventory(ClientProxy.configDir, this);
   }

   public void reset() {
      this.world = null;
      this.player = null;
   }

   private void init() {
      this.world = new FakeWorld();
      this.player = new FakePlayer(this.world, new GameProfile(this.uuid, this.username));
      InventoryPersistence.injectPersistedInventory(ClientProxy.configDir, this);
   }

   public EntityPlayer getPlayer() {
      if(this.player == null) {
         this.init();
      }

      return this.player;
   }

   public String getDisplayName() {
      return this.getPlayer().getDisplayName();
   }

   public World getWorld() {
      if(this.world == null) {
         this.init();
      }

      return this.world;
   }
}
