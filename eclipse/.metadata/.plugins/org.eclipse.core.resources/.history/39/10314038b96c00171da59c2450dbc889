package com.loaf.modulargun.weapons;

import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.loaf.modulargun.entity.EntityProjectile;
import com.loaf.modulargun.enums.EnumAmmoType;
import com.loaf.modulargun.enums.EnumFPHoldType;
import com.loaf.modulargun.fx.EntityFXMuzzleFlash;
import com.loaf.modulargun.main.Handler;
import com.loaf.modulargun.main.ModularGunMain;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ItemGun extends Item 
{	
	public int ammoAmount;
	public float recoil;
	public float fireDelay;
	public float shotDelay;
	public int reloadTime;
	public String gunName;
	public float damage;
	public boolean explosive;
	public int shootButtonId = 0;
	public int aimButtonId = 1;
	public static boolean isShootButtonDown;
	public static boolean isAimButtonDown;
	public static boolean isReloadKeyDown;
	public static boolean isCustomizeKeyDown;
	public EnumFPHoldType holdTypeFP;
	public EnumAmmoType ammoType;
	private EntityPlayer player;
	public float zoomAmt;
	public String gunSound;
	public String reloadSound;
	public String reloadSoundFull;
	
	public float minZoom = 7;
	public float maxZoom = 69;
	private int currentAmmo = 0;
	private float originalFOV = 69;
	private float currentRecoil = 0;
	private ItemAmmo ammo;
	public boolean isAiming;
	public boolean isShooting;	
	public boolean isReloading;
	public boolean isCustomizing;
	public float posX, posY, posZ;
	
	public boolean isZoomed = false;
	public ResourceLocation texture;
	private int zoomTick;
	private int reloadTick;
	private float recoilDelay;
	
	public ItemGun(String gunName, int ammoAmount, float recoil, float fireDelay, int reloadTime, float damage, EnumFPHoldType holdTypeFP, EnumAmmoType ammoType, float zoomAmt)
	{
		super();
		this.gunName = gunName;
		this.ammoAmount = ammoAmount;
		this.recoil = recoil;
		this.fireDelay = fireDelay;
		this.reloadTime = reloadTime;
		this.damage = damage;
		this.holdTypeFP = holdTypeFP;
		this.ammoType = ammoType;
		this.zoomAmt = zoomAmt;
		this.gunSound = "modulargun" + ":" + gunName;
		this.reloadSound = "modulargun" + ":" + gunName + "_reload";
		this.reloadSoundFull = "modulargun" + ":" + gunName + "_reloadfull";
		this.bFull3D = true; 
		this.setMaxStackSize(1);
		this.setCreativeTab(ModularGunMain.tabGuns);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int i, boolean isSelected)
	{
		if(entity instanceof EntityPlayer && ((EntityPlayer)entity).inventory.getCurrentItem() == stack) 
		{
			EntityPlayer player = (EntityPlayer)entity;
			if(stack.getTagCompound() == null) 
			{
				stack.stackTagCompound = new NBTTagCompound();
			}
			
			NBTTagCompound nbt = stack.stackTagCompound;
			
			isShootButtonDown = Mouse.isButtonDown(shootButtonId);
			isAimButtonDown = Mouse.isButtonDown(aimButtonId);
			isReloadKeyDown = Keyboard.isKeyDown(Keyboard.KEY_R);
			isCustomizeKeyDown = Keyboard.isKeyDown(Keyboard.KEY_C);
			
			int reloadTimer = nbt.getInteger("reloadTimer");
			
			if (reloadTimer > 0) 
			{
				nbt.setInteger("reloadTimer", reloadTimer - 1);
			}
			
			//Reload initializer.
			isReloading = false;
			if(this.needsToReload(stack) || isReloadKeyDown) 
			{
				if (reloadTimer > 0) 
				{
					nbt.setInteger("reloadTimer", reloadTimer);
					nbt.setInteger("reloadTimer", reloadTime);
				}
				else
				{
					reload(player, world, stack);
				}
			}
			
			//Keep the reloading animation going until reload is done.
			if (reloadTimer >= 1) 
			{
				isReloading = true;
			}
			
			fireDelay++;
			
			isShooting = false;
			
			//Shoot initializer.
			if(isShootButtonDown)
			{
				if(!(Minecraft.getMinecraft().currentScreen instanceof GuiScreen)) 
				{
					if(this.getAmmoAmount(stack) >= 1) 
					{
						if(reloadTimer <= 0) 
						{
							isShooting = true;
							shoot(stack, world, player);
							player.cameraPitch = recoil - recoil * 5;
							Minecraft.getMinecraft().thePlayer.renderArmPitch -= recoil * 6;
							this.setAmmoAmount(stack, getAmmoAmount(stack) - 1);
						}
					}
				}
			}
			
			//Aiming process.
			isAiming = false;
			if(isAimButtonDown) 
			{
				this.isAiming = true;
				Minecraft.getMinecraft().gameSettings.fovSetting = originalFOV - zoomAmt;
			}
			else 
			{
				Minecraft.getMinecraft().gameSettings.fovSetting = originalFOV + zoomAmt;
			}
		}
	}
	
	//Start shooting process.
	public ItemStack shoot(ItemStack stack, World world, EntityPlayer player) 
	{
		if (fireDelay >= shotDelay) 
		{
			if(!world.isRemote)
			{
				if(this.needsToReload(stack) == false) 
				{
					world.spawnEntityInWorld(new EntityProjectile(world, player, damage, explosive));
					world.playSoundAtEntity(player, gunSound, 0.3F, 1.0F);
					fireDelay = 0;
				}
			}
		}
		return stack;
	}
	
	//NBT Tags setters, and getters.
	public void setAmmoAmount(ItemStack stack, int ammo) {
		if (!stack.hasTagCompound())
			stack.setTagCompound(new NBTTagCompound());
		stack.getTagCompound().setInteger("ammoAmount", ammo);
	}
	
	public int getAmmoAmount(ItemStack stack) 
	{
		if(stack.hasTagCompound())
			return stack.getTagCompound().getInteger("ammoAmount");
		return currentAmmo;
	}
	
	public void setReloadTimer(ItemStack stack, int timer) {
		if (!stack.hasTagCompound())
			stack.setTagCompound(new NBTTagCompound());
		stack.getTagCompound().setInteger("reloadTimer", timer);
	}
	
	public int getReloadTimer(ItemStack stack) 
	{
		if(stack.hasTagCompound())
			return stack.getTagCompound().getInteger("reloadTimer");
		return reloadTime;
	}
	
	//Start reload process.
	public ItemStack reload(EntityPlayer player, World world, ItemStack stack) 
	{
		NBTTagCompound nbt = stack.stackTagCompound;
		isReloading = false;
		
		if(getAmmoAmount(stack) >= 1 && getAmmoAmount(stack) != ammoAmount) 
		{
			if(!world.isRemote) 
			{
				isReloading = true;
				world.playSoundAtEntity(player, reloadSoundFull, 1.0F, 1.0F);
				this.setAmmoAmount(stack, ammoAmount);
				nbt.setInteger("reloadTimer", reloadTime / 3);
			}
		}
		else if(getAmmoAmount(stack) == 0)
		{
			if(!world.isRemote) 
			{
				isReloading = true;
				world.playSoundAtEntity(player, reloadSound, 1.0F, 1.0F);
				this.setAmmoAmount(stack, ammoAmount);
				nbt.setInteger("reloadTimer", reloadTime);
			}
		}
		return stack;
	}
	
	//Override vanilla default item settings.
	@Override
    public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack)
    {
        return true;
    }
	
	@Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
        return true;
    }
	
	@Override
    public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z, EntityPlayer player)
    {
        return true;
    }
	
	@Override
    public boolean onBlockDestroyed(ItemStack p_150894_1_, World p_150894_2_, Block p_150894_3_, int p_150894_4_, int p_150894_5_, int p_150894_6_, EntityLivingBase p_150894_7_)
    {
        return true;
    }
	
	@Override
    public boolean func_150897_b(Block p_150897_1_)
    {
        return false;
    }
	
	//Check if gun needs to be reloaded.
	public boolean needsToReload(ItemStack stack) 
	{
		if(getAmmoAmount(stack) < 1) 
		{
			return true;
		}
		return false;
	}
	
	//Adding information below the gun's name.
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b) 
	{
		list.add("Ammo Count: " + Integer.toString(getAmmoAmount(stack)));
	}
}
