---- Minecraft Crash Report ----
// Don't be sad, have a hug! <3

Time: 7/2/17 7:36 PM
Description: Rendering item

java.lang.ClassCastException: com.lederpykewlaid.modulargun.item.guns.carbines.ModularM4Carbine cannot be cast to com.lederpykewlaid.modulargun.item.guns.ItemAttachment
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:47)
	at net.minecraftforge.client.ForgeHooksClient.renderInventoryItem(ForgeHooksClient.java:202)
	at net.minecraft.client.renderer.entity.RenderItem.renderItemAndEffectIntoGUI(RenderItem.java:583)
	at net.minecraft.client.gui.GuiIngame.renderInventorySlot(GuiIngame.java:973)
	at net.minecraftforge.client.GuiIngameForge.renderHotbar(GuiIngameForge.java:209)
	at net.minecraftforge.client.GuiIngameForge.renderGameOverlay(GuiIngameForge.java:144)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1114)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1067)
	at net.minecraft.client.Minecraft.run(Minecraft.java:962)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:47)
	at net.minecraftforge.client.ForgeHooksClient.renderInventoryItem(ForgeHooksClient.java:202)

-- Item being rendered --
Details:
	Item Type: com.lederpykewlaid.modulargun.item.guns.carbines.ModularM4Carbine@6c540ca5
	Item Aux: 0
	Item NBT: null
	Item Foil: false
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderItem.renderItemAndEffectIntoGUI(RenderItem.java:583)
	at net.minecraft.client.gui.GuiIngame.renderInventorySlot(GuiIngame.java:973)
	at net.minecraftforge.client.GuiIngameForge.renderHotbar(GuiIngameForge.java:209)
	at net.minecraftforge.client.GuiIngameForge.renderGameOverlay(GuiIngameForge.java:144)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Player257'/428, l='MpServer', x=-6.70, y=89.52, z=203.09]]
	Chunk stats: MultiplayerChunkCache: 20, 20
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (12,64,192), Chunk: (at 12,4,0 in 0,12; contains blocks 0,0,192 to 15,255,207), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 25220 game time, 10387 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 37 total; [EntitySpider['Spider'/192, l='MpServer', x=12.41, y=17.00, z=201.09], EntityBat['Bat'/193, l='MpServer', x=5.41, y=16.38, z=203.00], EntityBat['Bat'/194, l='MpServer', x=6.16, y=26.69, z=213.38], EntityBat['Bat'/195, l='MpServer', x=7.84, y=25.06, z=211.41], EntityBat['Bat'/196, l='MpServer', x=4.56, y=25.28, z=212.41], EntityMinecartChest['entity.MinecartChest.name'/197, l='MpServer', x=12.50, y=13.50, z=236.50], EntitySkeleton['Skeleton'/198, l='MpServer', x=14.50, y=21.00, z=235.50], EntitySquid['Squid'/199, l='MpServer', x=0.06, y=53.38, z=239.84], EntitySheep['Sheep'/145, l='MpServer', x=-21.47, y=63.00, z=188.13], EntityXPOrb['Experience Orb'/146, l='MpServer', x=-31.81, y=40.13, z=225.69], EntitySpider['Spider'/166, l='MpServer', x=-5.06, y=35.00, z=182.88], EntityZombie['Zombie'/167, l='MpServer', x=-6.69, y=35.00, z=178.00], EntitySpider['Spider'/168, l='MpServer', x=-9.50, y=41.00, z=201.50], EntityPig['Pig'/169, l='MpServer', x=-2.53, y=76.00, z=193.44], EntitySkeleton['Skeleton'/170, l='MpServer', x=-3.50, y=48.00, z=213.50], EntityZombie['Zombie'/234, l='MpServer', x=21.50, y=7.00, z=207.50], EntitySquid['Squid'/106, l='MpServer', x=-46.75, y=58.34, z=200.25], EntityWitch['Witch'/235, l='MpServer', x=17.50, y=13.00, z=221.50], EntityZombie['Zombie'/171, l='MpServer', x=-14.66, y=27.00, z=227.06], EntityItem['item.item.dyePowder.black'/107, l='MpServer', x=-34.81, y=41.13, z=217.09], EntitySkeleton['Skeleton'/236, l='MpServer', x=16.44, y=13.00, z=229.72], EntityXPOrb['Experience Orb'/108, l='MpServer', x=-37.66, y=41.13, z=212.53], EntityClientPlayerMP['Player257'/428, l='MpServer', x=-6.70, y=89.52, z=203.09], EntitySkeleton['Skeleton'/237, l='MpServer', x=16.34, y=13.00, z=232.31], EntityXPOrb['Experience Orb'/109, l='MpServer', x=-38.97, y=40.13, z=217.00], EntityItem['item.item.dyePowder.black'/110, l='MpServer', x=-36.72, y=41.13, z=227.53], EntityItem['item.item.dyePowder.black'/111, l='MpServer', x=-35.91, y=41.13, z=231.38], EntityXPOrb['Experience Orb'/112, l='MpServer', x=-40.06, y=41.13, z=232.56], EntityItem['item.item.dyePowder.black'/113, l='MpServer', x=-40.13, y=41.13, z=233.47], EntityXPOrb['Experience Orb'/114, l='MpServer', x=-34.19, y=41.13, z=233.72], EntityXPOrb['Experience Orb'/115, l='MpServer', x=-37.19, y=41.13, z=231.78], EntityXPOrb['Experience Orb'/116, l='MpServer', x=-37.47, y=41.13, z=235.75], EntityXPOrb['Experience Orb'/117, l='MpServer', x=-34.63, y=41.13, z=235.31], EntitySquid['Squid'/118, l='MpServer', x=-40.38, y=56.09, z=228.50], EntitySquid['Squid'/119, l='MpServer', x=-38.53, y=51.34, z=236.63], EntitySquid['Squid'/120, l='MpServer', x=-40.84, y=57.00, z=226.91], EntitySquid['Squid'/121, l='MpServer', x=-47.94, y=59.31, z=233.09]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:415)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2566)
	at net.minecraft.client.Minecraft.run(Minecraft.java:984)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 798120000 bytes (761 MB) / 1038876672 bytes (990 MB) up to 1038876672 bytes (990 MB)
	JVM Flags: 3 total; -Xincgc -Xmx1024M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 12, tallocated: 94
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modularguns{1.0} [modularguns] (bin) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 382.05' Renderer: 'GeForce GTX 1070/PCIe/SSE2'
	Launched Version: 1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 1070/PCIe/SSE2 GL version 4.5.0 NVIDIA 382.05, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)