---- Minecraft Crash Report ----
// Shall we play a game?

Time: 7/6/17 6:44 PM
Description: Ticking player

java.lang.NullPointerException: Ticking player
	at com.lederpykewlaid.modulargun.guns.ModularGun.onUpdate(ModularGun.java:96)
	at net.minecraft.item.ItemStack.updateAnimation(ItemStack.java:476)
	at net.minecraft.entity.player.InventoryPlayer.decrementAnimations(InventoryPlayer.java:347)
	at net.minecraft.entity.player.EntityPlayer.onLivingUpdate(EntityPlayer.java:610)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:1816)
	at net.minecraft.entity.player.EntityPlayer.onUpdate(EntityPlayer.java:327)
	at net.minecraft.entity.player.EntityPlayerMP.onUpdateEntity(EntityPlayerMP.java:330)
	at net.minecraft.network.NetHandlerPlayServer.processPlayer(NetHandlerPlayServer.java:329)
	at net.minecraft.network.play.client.C03PacketPlayer.processPacket(C03PacketPlayer.java:37)
	at net.minecraft.network.play.client.C03PacketPlayer$C06PacketPlayerPosLook.processPacket(C03PacketPlayer.java:271)
	at net.minecraft.network.NetworkManager.processReceivedPackets(NetworkManager.java:241)
	at net.minecraft.network.NetworkSystem.networkTick(NetworkSystem.java:182)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:726)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:614)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:118)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:485)
	at net.minecraft.server.MinecraftServer$2.run(MinecraftServer.java:752)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.lederpykewlaid.modulargun.guns.ModularGun.onUpdate(ModularGun.java:96)
	at net.minecraft.item.ItemStack.updateAnimation(ItemStack.java:476)
	at net.minecraft.entity.player.InventoryPlayer.decrementAnimations(InventoryPlayer.java:347)
	at net.minecraft.entity.player.EntityPlayer.onLivingUpdate(EntityPlayer.java:610)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:1816)
	at net.minecraft.entity.player.EntityPlayer.onUpdate(EntityPlayer.java:327)

-- Player being ticked --
Details:
	Entity Type: null (net.minecraft.entity.player.EntityPlayerMP)
	Entity ID: 318
	Entity Name: Player958
	Entity's Exact location: 266.83, 75.64, 286.16
	Entity's Block location: World: (266,75,286), Chunk: (at 10,4,14 in 16,17; contains blocks 256,0,272 to 271,255,287), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Entity's Momentum: 0.00, 0.00, 0.00
Stacktrace:
	at net.minecraft.entity.player.EntityPlayerMP.onUpdateEntity(EntityPlayerMP.java:330)
	at net.minecraft.network.NetHandlerPlayServer.processPlayer(NetHandlerPlayServer.java:329)
	at net.minecraft.network.play.client.C03PacketPlayer.processPacket(C03PacketPlayer.java:37)
	at net.minecraft.network.play.client.C03PacketPlayer$C06PacketPlayerPosLook.processPacket(C03PacketPlayer.java:271)
	at net.minecraft.network.NetworkManager.processReceivedPackets(NetworkManager.java:241)

-- Ticking connection --
Details:
	Connection: net.minecraft.network.NetworkManager@628b4925
Stacktrace:
	at net.minecraft.network.NetworkSystem.networkTick(NetworkSystem.java:182)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:726)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:614)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:118)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:485)
	at net.minecraft.server.MinecraftServer$2.run(MinecraftServer.java:752)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 917676928 bytes (875 MB) / 1038876672 bytes (990 MB) up to 2112618496 bytes (2014 MB)
	JVM Flags: 3 total; -Xincgc -Xmx2048M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 15, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modulargun{v1.32.3a} [Modular Guns Mod] (bin) 
	GL info: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Player Count: 1 / 8; [EntityPlayerMP['Player958'/318, l='Testing', x=266.83, y=75.64, z=286.16]]
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fml,forge'