---- Minecraft Crash Report ----
// Uh... Did I do that?

Time: 7/5/17 8:56 PM
Description: Unexpected error

java.lang.NullPointerException: Unexpected error
	at com.lederpykewlaid.modulargun.render.RenderGun.renderShell(RenderGun.java:273)
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:71)
	at net.minecraftforge.client.ForgeHooksClient.renderEquippedItem(ForgeHooksClient.java:232)
	at net.minecraft.client.renderer.ItemRenderer.renderItem(ItemRenderer.java:86)
	at net.minecraft.client.renderer.ItemRenderer.renderItemInFirstPerson(ItemRenderer.java:511)
	at net.minecraft.client.renderer.EntityRenderer.renderHand(EntityRenderer.java:797)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1437)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1091)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1067)
	at net.minecraft.client.Minecraft.run(Minecraft.java:962)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.lederpykewlaid.modulargun.render.RenderGun.renderShell(RenderGun.java:273)
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:71)
	at net.minecraftforge.client.ForgeHooksClient.renderEquippedItem(ForgeHooksClient.java:232)
	at net.minecraft.client.renderer.ItemRenderer.renderItem(ItemRenderer.java:86)
	at net.minecraft.client.renderer.ItemRenderer.renderItemInFirstPerson(ItemRenderer.java:511)
	at net.minecraft.client.renderer.EntityRenderer.renderHand(EntityRenderer.java:797)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1437)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Player366'/208, l='MpServer', x=-77.01, y=72.73, z=-7.80]]
	Chunk stats: MultiplayerChunkCache: 65, 65
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (212,64,256), Chunk: (at 4,4,0 in 13,16; contains blocks 208,0,256 to 223,255,271), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 61787 game time, 27069 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 55 total; [EntityChicken['Chicken'/256, l='MpServer', x=-33.44, y=68.46, z=56.56], EntitySkeleton['Skeleton'/257, l='MpServer', x=-115.56, y=19.15, z=-69.22], EntityChicken['Chicken'/258, l='MpServer', x=-17.38, y=69.46, z=50.47], EntityChicken['Chicken'/259, l='MpServer', x=-82.44, y=67.46, z=-86.59], EntityChicken['Chicken'/261, l='MpServer', x=-49.44, y=68.46, z=72.44], EntitySkeleton['Skeleton'/262, l='MpServer', x=-152.50, y=17.15, z=29.50], EntityBat['Bat'/263, l='MpServer', x=-152.85, y=17.47, z=26.97], EntityBat['Bat'/264, l='MpServer', x=-153.44, y=32.14, z=34.65], EntityBat['Bat'/265, l='MpServer', x=-154.75, y=31.84, z=39.13], EntityMinecartChest['entity.MinecartChest.name'/267, l='MpServer', x=2.50, y=33.50, z=40.50], EntitySkeleton['Skeleton'/209, l='MpServer', x=-70.38, y=49.00, z=-30.03], EntityCreeper['Creeper'/210, l='MpServer', x=-66.88, y=48.00, z=-30.88], EntityClientPlayerMP['Player366'/208, l='MpServer', x=-77.01, y=72.73, z=-7.80], EntityChicken['Chicken'/212, l='MpServer', x=-56.44, y=68.36, z=-8.41], EntityCreeper['Creeper'/215, l='MpServer', x=-59.30, y=49.67, z=-28.43], EntityCreeper['Creeper'/216, l='MpServer', x=-58.39, y=49.67, z=-29.03], EntityCreeper['Creeper'/217, l='MpServer', x=-59.53, y=49.00, z=-29.56], EntityZombie['Zombie'/218, l='MpServer', x=-55.97, y=49.00, z=-30.44], EntityEnderman['Enderman'/219, l='MpServer', x=-103.85, y=41.36, z=-11.04], EntityCreeper['Creeper'/220, l='MpServer', x=-76.53, y=45.00, z=-39.66], EntityCreeper['Creeper'/221, l='MpServer', x=-73.36, y=47.52, z=-35.07], EntityChicken['Chicken'/222, l='MpServer', x=-78.41, y=68.36, z=-43.53], EntityCreeper['Creeper'/223, l='MpServer', x=-41.69, y=53.00, z=-3.90], EntityCreeper['Creeper'/224, l='MpServer', x=-43.50, y=53.00, z=-3.50], EntityCreeper['Creeper'/225, l='MpServer', x=-43.50, y=53.00, z=-4.50], EntitySkeleton['Skeleton'/226, l='MpServer', x=-101.50, y=39.00, z=-25.50], EntitySkeleton['Skeleton'/227, l='MpServer', x=-101.50, y=39.00, z=-26.50], EntitySkeleton['Skeleton'/228, l='MpServer', x=-101.50, y=39.00, z=-29.34], EntityCreeper['Creeper'/229, l='MpServer', x=-99.50, y=39.00, z=-27.50], EntityCreeper['Creeper'/230, l='MpServer', x=-101.50, y=39.00, z=-30.41], EntityZombie['Zombie'/231, l='MpServer', x=-89.22, y=48.00, z=-36.63], EntityCreeper['Creeper'/232, l='MpServer', x=-40.50, y=52.00, z=-18.35], EntityCreeper['Creeper'/233, l='MpServer', x=-42.00, y=53.00, z=-20.53], EntityBat['Bat'/234, l='MpServer', x=-44.66, y=59.10, z=-31.75], EntityCreeper['Creeper'/235, l='MpServer', x=-100.50, y=39.00, z=-33.50], EntityWitch['Witch'/236, l='MpServer', x=-37.50, y=49.00, z=27.50], EntityBat['Bat'/237, l='MpServer', x=-66.64, y=42.90, z=-55.60], EntityBat['Bat'/238, l='MpServer', x=-114.75, y=22.10, z=-25.44], EntityBat['Bat'/239, l='MpServer', x=-116.56, y=25.11, z=-22.57], EntityCreeper['Creeper'/240, l='MpServer', x=-113.50, y=34.00, z=-29.50], EntityCreeper['Creeper'/241, l='MpServer', x=-116.15, y=34.00, z=-19.56], EntityZombie['Zombie'/242, l='MpServer', x=-88.50, y=46.00, z=-52.50], EntityCreeper['Creeper'/243, l='MpServer', x=-89.56, y=53.00, z=-53.03], EntityChicken['Chicken'/244, l='MpServer', x=-87.59, y=69.41, z=-50.44], EntityChicken['Chicken'/245, l='MpServer', x=-23.23, y=68.76, z=-24.48], EntityCreeper['Creeper'/246, l='MpServer', x=-120.50, y=24.08, z=-45.50], EntitySkeleton['Skeleton'/247, l='MpServer', x=-35.50, y=42.08, z=42.50], EntityChicken['Chicken'/248, l='MpServer', x=-43.44, y=68.44, z=43.56], EntityChicken['Chicken'/249, l='MpServer', x=-12.66, y=68.44, z=-8.84], EntityChicken['Chicken'/250, l='MpServer', x=-12.41, y=70.44, z=-9.59], EntityCreeper['Creeper'/251, l='MpServer', x=-70.50, y=12.15, z=55.50], EntityChicken['Chicken'/252, l='MpServer', x=-69.53, y=70.46, z=57.53], EntityZombie['Zombie'/253, l='MpServer', x=-62.88, y=12.15, z=48.56], EntityChicken['Chicken'/254, l='MpServer', x=-41.47, y=70.46, z=-77.47], EntityChicken['Chicken'/255, l='MpServer', x=-8.47, y=78.46, z=17.66]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:415)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2566)
	at net.minecraft.client.Minecraft.run(Minecraft.java:991)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 677712472 bytes (646 MB) / 1038876672 bytes (990 MB) up to 2112618496 bytes (2014 MB)
	JVM Flags: 3 total; -Xincgc -Xmx2048M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 13, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modulargun{v1.32.0a} [Modular Guns Mod] (bin) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 382.05' Renderer: 'GeForce GTX 1070/PCIe/SSE2'
	Launched Version: 1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 1070/PCIe/SSE2 GL version 4.5.0 NVIDIA 382.05, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)