---- Minecraft Crash Report ----
// I feel sad now :(

Time: 7/6/17 6:01 PM
Description: Ticking entity

java.lang.NullPointerException: Ticking entity
	at com.lederpykewlaid.modulargun.guns.ModularGun.onUpdate(ModularGun.java:97)
	at net.minecraft.item.ItemStack.updateAnimation(ItemStack.java:476)
	at net.minecraft.entity.player.InventoryPlayer.decrementAnimations(InventoryPlayer.java:347)
	at net.minecraft.entity.player.EntityPlayer.onLivingUpdate(EntityPlayer.java:610)
	at net.minecraft.client.entity.EntityPlayerSP.onLivingUpdate(EntityPlayerSP.java:299)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:1816)
	at net.minecraft.entity.player.EntityPlayer.onUpdate(EntityPlayer.java:327)
	at net.minecraft.client.entity.EntityClientPlayerMP.onUpdate(EntityClientPlayerMP.java:96)
	at net.minecraft.world.World.updateEntityWithOptionalForce(World.java:2298)
	at net.minecraft.world.World.updateEntity(World.java:2258)
	at net.minecraft.world.World.updateEntities(World.java:2108)
	at net.minecraft.client.Minecraft.runTick(Minecraft.java:2097)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1039)
	at net.minecraft.client.Minecraft.run(Minecraft.java:962)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.lederpykewlaid.modulargun.guns.ModularGun.onUpdate(ModularGun.java:97)
	at net.minecraft.item.ItemStack.updateAnimation(ItemStack.java:476)
	at net.minecraft.entity.player.InventoryPlayer.decrementAnimations(InventoryPlayer.java:347)
	at net.minecraft.entity.player.EntityPlayer.onLivingUpdate(EntityPlayer.java:610)
	at net.minecraft.client.entity.EntityPlayerSP.onLivingUpdate(EntityPlayerSP.java:299)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:1816)
	at net.minecraft.entity.player.EntityPlayer.onUpdate(EntityPlayer.java:327)
	at net.minecraft.client.entity.EntityClientPlayerMP.onUpdate(EntityClientPlayerMP.java:96)
	at net.minecraft.world.World.updateEntityWithOptionalForce(World.java:2298)
	at net.minecraft.world.World.updateEntity(World.java:2258)

-- Entity being ticked --
Details:
	Entity Type: null (net.minecraft.client.entity.EntityClientPlayerMP)
	Entity ID: 313
	Entity Name: Player558
	Entity's Exact location: 223.23, 72.62, 253.24
	Entity's Block location: World: (223,72,253), Chunk: (at 15,4,13 in 13,15; contains blocks 208,0,240 to 223,255,255), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Entity's Momentum: 0.00, -0.08, 0.00
Stacktrace:
	at net.minecraft.world.World.updateEntities(World.java:2108)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Player558'/313, l='MpServer', x=223.23, y=72.62, z=253.24]]
	Chunk stats: MultiplayerChunkCache: 289, 289
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (212,64,256), Chunk: (at 4,4,0 in 13,16; contains blocks 208,0,256 to 223,255,271), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 76503 game time, 27069 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 106 total; [EntityCow['Cow'/256, l='MpServer', x=301.38, y=68.00, z=317.38], EntityCow['Cow'/257, l='MpServer', x=288.75, y=69.00, z=332.44], EntityClientPlayerMP['Player558'/313, l='MpServer', x=223.23, y=72.62, z=253.24], EntityZombie['Zombie'/82, l='MpServer', x=152.41, y=11.00, z=196.00], EntityZombie['Zombie'/83, l='MpServer', x=149.50, y=53.00, z=206.50], EntityBat['Bat'/84, l='MpServer', x=154.25, y=30.10, z=233.38], EntityCreeper['Creeper'/85, l='MpServer', x=150.50, y=63.00, z=286.50], EntityCreeper['Creeper'/86, l='MpServer', x=152.50, y=63.00, z=282.50], EntityZombie['Zombie'/87, l='MpServer', x=152.50, y=29.00, z=295.50], EntityCreeper['Creeper'/88, l='MpServer', x=151.50, y=63.00, z=289.50], EntityCreeper['Creeper'/89, l='MpServer', x=152.72, y=63.00, z=286.44], EntityZombie['Zombie'/90, l='MpServer', x=152.50, y=13.00, z=332.16], EntityCreeper['Creeper'/96, l='MpServer', x=176.04, y=68.00, z=174.50], EntityZombie['Zombie'/99, l='MpServer', x=164.97, y=16.00, z=188.50], EntityCreeper['Creeper'/100, l='MpServer', x=173.69, y=50.00, z=191.91], EntityCow['Cow'/101, l='MpServer', x=171.78, y=77.00, z=185.64], EntityCow['Cow'/102, l='MpServer', x=171.03, y=77.00, z=187.16], EntityCow['Cow'/103, l='MpServer', x=173.53, y=84.00, z=180.38], EntityChicken['Chicken'/104, l='MpServer', x=170.59, y=89.00, z=182.44], EntitySkeleton['Skeleton'/105, l='MpServer', x=165.91, y=14.00, z=197.29], EntityZombie['Zombie'/106, l='MpServer', x=165.06, y=14.00, z=196.69], EntityZombie['Zombie'/107, l='MpServer', x=164.31, y=14.00, z=195.75], EntitySpider['Spider'/108, l='MpServer', x=174.25, y=61.00, z=207.16], EntitySkeleton['Skeleton'/109, l='MpServer', x=167.31, y=58.00, z=201.02], EntitySkeleton['Skeleton'/110, l='MpServer', x=169.50, y=59.00, z=202.51], EntitySkeleton['Skeleton'/111, l='MpServer', x=168.53, y=21.00, z=208.56], EntityBat['Bat'/112, l='MpServer', x=171.56, y=53.10, z=219.75], EntitySpider['Spider'/113, l='MpServer', x=166.70, y=50.02, z=214.70], EntityHorse['Horse'/114, l='MpServer', x=171.97, y=82.00, z=213.66], EntityZombie['Zombie'/115, l='MpServer', x=175.78, y=30.00, z=227.22], EntityZombie['Zombie'/116, l='MpServer', x=164.50, y=29.00, z=240.03], EntityBat['Bat'/117, l='MpServer', x=173.94, y=57.14, z=249.47], EntityZombie['Zombie'/128, l='MpServer', x=180.15, y=67.00, z=177.99], EntityBat['Bat'/129, l='MpServer', x=190.75, y=16.10, z=197.75], EntityCreeper['Creeper'/130, l='MpServer', x=182.50, y=60.00, z=192.50], EntityZombie['Zombie'/131, l='MpServer', x=181.50, y=52.00, z=220.50], EntityZombie['Zombie'/132, l='MpServer', x=180.50, y=52.00, z=220.50], EntityZombie['Zombie'/133, l='MpServer', x=182.50, y=52.00, z=221.50], EntityCreeper['Creeper'/134, l='MpServer', x=186.50, y=50.00, z=213.50], EntityZombie['Zombie'/135, l='MpServer', x=191.75, y=20.00, z=328.50], EntityBat['Bat'/151, l='MpServer', x=200.34, y=23.10, z=180.25], EntitySkeleton['Skeleton'/152, l='MpServer', x=193.50, y=63.00, z=198.88], EntityBat['Bat'/153, l='MpServer', x=200.22, y=57.01, z=208.28], EntityBat['Bat'/154, l='MpServer', x=202.44, y=47.10, z=209.13], EntitySkeleton['Skeleton'/155, l='MpServer', x=188.20, y=50.00, z=217.50], EntitySkeleton['Skeleton'/156, l='MpServer', x=192.50, y=50.00, z=216.50], EntityBat['Bat'/157, l='MpServer', x=199.28, y=58.10, z=209.75], EntityMinecartChest['entity.MinecartChest.name'/158, l='MpServer', x=193.50, y=37.50, z=231.50], EntityZombie['Zombie'/159, l='MpServer', x=201.50, y=15.00, z=270.50], EntitySkeleton['Skeleton'/160, l='MpServer', x=207.09, y=40.00, z=314.56], EntityBat['Bat'/161, l='MpServer', x=206.75, y=42.10, z=313.09], EntitySkeleton['Skeleton'/162, l='MpServer', x=198.50, y=20.00, z=325.50], EntityZombie['Zombie'/163, l='MpServer', x=193.70, y=20.00, z=328.50], EntityZombie['Zombie'/164, l='MpServer', x=195.50, y=20.00, z=326.50], EntitySheep['Sheep'/165, l='MpServer', x=198.13, y=69.00, z=329.78], EntityZombie['Zombie'/176, l='MpServer', x=209.34, y=68.00, z=175.16], EntityCreeper['Creeper'/179, l='MpServer', x=223.50, y=53.00, z=205.50], EntitySquid['Squid'/180, l='MpServer', x=216.92, y=61.00, z=207.90], EntityChicken['Chicken'/181, l='MpServer', x=223.41, y=64.00, z=203.47], EntityZombie['Zombie'/182, l='MpServer', x=218.50, y=38.00, z=221.50], EntityZombie['Zombie'/183, l='MpServer', x=219.59, y=38.00, z=223.06], EntityCreeper['Creeper'/184, l='MpServer', x=210.75, y=51.00, z=216.50], EntitySkeleton['Skeleton'/185, l='MpServer', x=217.26, y=51.00, z=218.22], EntitySkeleton['Skeleton'/186, l='MpServer', x=211.66, y=51.00, z=215.91], EntitySquid['Squid'/187, l='MpServer', x=224.53, y=61.39, z=222.93], EntityCreeper['Creeper'/188, l='MpServer', x=216.50, y=14.00, z=326.50], EntityBat['Bat'/189, l='MpServer', x=218.63, y=51.10, z=324.13], EntitySpider['Spider'/190, l='MpServer', x=209.28, y=47.00, z=328.97], EntityChicken['Chicken'/191, l='MpServer', x=216.71, y=71.00, z=334.27], EntityChicken['Chicken'/198, l='MpServer', x=236.59, y=70.00, z=174.56], EntityChicken['Chicken'/199, l='MpServer', x=225.66, y=35.00, z=187.47], EntityChicken['Chicken'/200, l='MpServer', x=228.53, y=80.00, z=182.59], EntitySpider['Spider'/201, l='MpServer', x=237.50, y=33.00, z=220.50], EntityZombie['Zombie'/202, l='MpServer', x=239.50, y=53.00, z=212.50], EntityCreeper['Creeper'/203, l='MpServer', x=237.00, y=21.00, z=291.41], EntityCreeper['Creeper'/204, l='MpServer', x=237.46, y=21.00, z=294.88], EntityCow['Cow'/205, l='MpServer', x=232.94, y=70.00, z=297.81], EntityChicken['Chicken'/206, l='MpServer', x=231.47, y=70.00, z=290.19], EntityChicken['Chicken'/207, l='MpServer', x=228.44, y=71.00, z=298.56], EntityCow['Cow'/208, l='MpServer', x=238.66, y=70.00, z=307.72], EntityWitch['Witch'/219, l='MpServer', x=249.78, y=24.00, z=223.78], EntitySpider['Spider'/220, l='MpServer', x=240.50, y=24.00, z=239.50], EntityBat['Bat'/221, l='MpServer', x=258.06, y=22.07, z=239.41], EntityBat['Bat'/222, l='MpServer', x=245.50, y=32.10, z=262.25], EntityCreeper['Creeper'/223, l='MpServer', x=246.28, y=22.00, z=288.09], EntityZombie['Zombie'/224, l='MpServer', x=240.50, y=21.00, z=288.50], EntitySkeleton['Skeleton'/225, l='MpServer', x=241.97, y=55.00, z=305.44], EntitySheep['Sheep'/226, l='MpServer', x=253.16, y=70.00, z=328.91], EntitySkeleton['Skeleton'/231, l='MpServer', x=265.16, y=25.00, z=182.50], EntityChicken['Chicken'/232, l='MpServer', x=266.31, y=62.51, z=197.47], EntitySkeleton['Skeleton'/233, l='MpServer', x=257.63, y=24.00, z=216.84], EntityWitch['Witch'/234, l='MpServer', x=258.25, y=24.00, z=222.25], EntitySkeleton['Skeleton'/235, l='MpServer', x=264.88, y=21.00, z=242.50], EntityCow['Cow'/236, l='MpServer', x=268.06, y=72.00, z=285.06], EntityCow['Cow'/237, l='MpServer', x=270.09, y=71.00, z=294.06], EntityZombie['Zombie'/238, l='MpServer', x=259.50, y=16.00, z=312.50], EntityCow['Cow'/244, l='MpServer', x=284.03, y=72.00, z=213.16], EntityCow['Cow'/245, l='MpServer', x=280.72, y=70.00, z=230.50], EntitySkeleton['Skeleton'/246, l='MpServer', x=276.44, y=19.00, z=243.31], EntityCow['Cow'/247, l='MpServer', x=283.84, y=72.00, z=283.59], EntityCow['Cow'/248, l='MpServer', x=287.09, y=72.00, z=283.81], EntityCow['Cow'/249, l='MpServer', x=276.88, y=69.00, z=325.19], EntityCow['Cow'/252, l='MpServer', x=299.88, y=71.00, z=188.78], EntityCow['Cow'/253, l='MpServer', x=288.91, y=70.00, z=194.81], EntityZombie['Zombie'/254, l='MpServer', x=291.16, y=20.00, z=246.50], EntityCow['Cow'/255, l='MpServer', x=290.88, y=70.00, z=294.97]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:415)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2566)
	at net.minecraft.client.Minecraft.run(Minecraft.java:984)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 821725304 bytes (783 MB) / 1038876672 bytes (990 MB) up to 2112618496 bytes (2014 MB)
	JVM Flags: 3 total; -Xincgc -Xmx2048M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 15, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modulargun{v1.32.3a} [Modular Guns Mod] (bin) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 382.05' Renderer: 'GeForce GTX 1070/PCIe/SSE2'
	Launched Version: 1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 1070/PCIe/SSE2 GL version 4.5.0 NVIDIA 382.05, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)