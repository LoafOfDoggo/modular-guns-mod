---- Minecraft Crash Report ----
// I just don't know what went wrong :(

Time: 7/5/17 8:54 PM
Description: Unexpected error

java.lang.NullPointerException: Unexpected error
	at com.lederpykewlaid.modulargun.render.RenderGun.renderShell(RenderGun.java:272)
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:70)
	at net.minecraftforge.client.ForgeHooksClient.renderEquippedItem(ForgeHooksClient.java:232)
	at net.minecraft.client.renderer.ItemRenderer.renderItem(ItemRenderer.java:86)
	at net.minecraft.client.renderer.ItemRenderer.renderItemInFirstPerson(ItemRenderer.java:511)
	at net.minecraft.client.renderer.EntityRenderer.renderHand(EntityRenderer.java:797)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1437)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1091)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1067)
	at net.minecraft.client.Minecraft.run(Minecraft.java:962)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.lederpykewlaid.modulargun.render.RenderGun.renderShell(RenderGun.java:272)
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:70)
	at net.minecraftforge.client.ForgeHooksClient.renderEquippedItem(ForgeHooksClient.java:232)
	at net.minecraft.client.renderer.ItemRenderer.renderItem(ItemRenderer.java:86)
	at net.minecraft.client.renderer.ItemRenderer.renderItemInFirstPerson(ItemRenderer.java:511)
	at net.minecraft.client.renderer.EntityRenderer.renderHand(EntityRenderer.java:797)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1437)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Player398'/208, l='MpServer', x=-77.01, y=72.73, z=-7.80]]
	Chunk stats: MultiplayerChunkCache: 120, 120
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (212,64,256), Chunk: (at 4,4,0 in 13,16; contains blocks 208,0,256 to 223,255,271), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 61735 game time, 27069 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 53 total; [EntityWitch['Witch'/258, l='MpServer', x=-37.50, y=49.00, z=27.50], EntityBat['Bat'/259, l='MpServer', x=-66.14, y=43.11, z=-54.30], EntityBat['Bat'/260, l='MpServer', x=-114.75, y=22.10, z=-25.44], EntityBat['Bat'/261, l='MpServer', x=-115.95, y=25.68, z=-20.43], EntityCreeper['Creeper'/262, l='MpServer', x=-113.50, y=34.00, z=-29.50], EntityCreeper['Creeper'/263, l='MpServer', x=-117.50, y=34.00, z=-19.50], EntityZombie['Zombie'/264, l='MpServer', x=-88.50, y=46.00, z=-52.50], EntityCreeper['Creeper'/265, l='MpServer', x=-89.56, y=53.00, z=-53.03], EntityChicken['Chicken'/266, l='MpServer', x=-87.59, y=69.00, z=-50.44], EntityChicken['Chicken'/267, l='MpServer', x=-22.47, y=68.00, z=-24.53], EntityCreeper['Creeper'/268, l='MpServer', x=-120.50, y=21.00, z=-45.50], EntitySkeleton['Skeleton'/269, l='MpServer', x=-35.50, y=42.00, z=42.50], EntityChicken['Chicken'/270, l='MpServer', x=-43.44, y=68.00, z=43.56], EntityChicken['Chicken'/271, l='MpServer', x=-12.66, y=68.00, z=-8.84], EntityChicken['Chicken'/272, l='MpServer', x=-12.41, y=69.44, z=-9.59], EntityCreeper['Creeper'/273, l='MpServer', x=-70.50, y=12.00, z=55.50], EntityChicken['Chicken'/274, l='MpServer', x=-69.53, y=70.00, z=57.53], EntityZombie['Zombie'/275, l='MpServer', x=-62.88, y=12.00, z=48.56], EntityChicken['Chicken'/276, l='MpServer', x=-41.47, y=70.00, z=-77.47], EntityChicken['Chicken'/277, l='MpServer', x=-8.47, y=78.00, z=17.66], EntityChicken['Chicken'/278, l='MpServer', x=-33.44, y=68.00, z=56.56], EntitySkeleton['Skeleton'/279, l='MpServer', x=-115.56, y=18.00, z=-69.22], EntityChicken['Chicken'/280, l='MpServer', x=-17.38, y=69.00, z=50.47], EntityChicken['Chicken'/281, l='MpServer', x=-82.44, y=67.00, z=-86.59], EntityChicken['Chicken'/283, l='MpServer', x=-49.44, y=68.00, z=72.44], EntitySkeleton['Skeleton'/284, l='MpServer', x=-152.50, y=16.00, z=29.50], EntityBat['Bat'/285, l='MpServer', x=-154.36, y=17.62, z=28.57], EntityMinecartChest['entity.MinecartChest.name'/287, l='MpServer', x=2.50, y=33.50, z=40.50], EntityClientPlayerMP['Player398'/208, l='MpServer', x=-77.01, y=72.73, z=-7.80], EntitySkeleton['Skeleton'/210, l='MpServer', x=-70.38, y=49.00, z=-30.03], EntityCreeper['Creeper'/211, l='MpServer', x=-66.88, y=48.00, z=-30.88], EntityChicken['Chicken'/212, l='MpServer', x=-56.44, y=69.00, z=-8.41], EntityCreeper['Creeper'/213, l='MpServer', x=-59.38, y=50.00, z=-28.31], EntityCreeper['Creeper'/214, l='MpServer', x=-58.50, y=50.00, z=-28.97], EntityCreeper['Creeper'/215, l='MpServer', x=-59.53, y=50.00, z=-29.56], EntityZombie['Zombie'/216, l='MpServer', x=-55.97, y=50.00, z=-30.44], EntityEnderman['Enderman'/221, l='MpServer', x=-104.47, y=42.00, z=-10.84], EntityCreeper['Creeper'/223, l='MpServer', x=-76.53, y=46.00, z=-39.66], EntityCreeper['Creeper'/224, l='MpServer', x=-73.50, y=48.00, z=-35.50], EntityChicken['Chicken'/225, l='MpServer', x=-78.41, y=69.00, z=-43.53], EntityCreeper['Creeper'/226, l='MpServer', x=-41.50, y=54.00, z=-4.50], EntityCreeper['Creeper'/227, l='MpServer', x=-43.50, y=54.00, z=-3.50], EntityCreeper['Creeper'/228, l='MpServer', x=-43.50, y=54.00, z=-4.50], EntitySkeleton['Skeleton'/229, l='MpServer', x=-101.50, y=40.00, z=-25.50], EntitySkeleton['Skeleton'/230, l='MpServer', x=-101.50, y=40.00, z=-26.50], EntitySkeleton['Skeleton'/231, l='MpServer', x=-101.50, y=40.00, z=-29.49], EntityCreeper['Creeper'/232, l='MpServer', x=-99.50, y=40.00, z=-27.50], EntityCreeper['Creeper'/233, l='MpServer', x=-101.50, y=40.00, z=-30.41], EntityZombie['Zombie'/234, l='MpServer', x=-89.78, y=50.00, z=-36.41], EntityCreeper['Creeper'/238, l='MpServer', x=-40.50, y=52.00, z=-19.50], EntityCreeper['Creeper'/240, l='MpServer', x=-42.00, y=53.00, z=-20.53], EntityBat['Bat'/243, l='MpServer', x=-44.66, y=59.10, z=-31.75], EntityCreeper['Creeper'/250, l='MpServer', x=-100.50, y=39.00, z=-33.50]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:415)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2566)
	at net.minecraft.client.Minecraft.run(Minecraft.java:991)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 645417320 bytes (615 MB) / 1038876672 bytes (990 MB) up to 2112618496 bytes (2014 MB)
	JVM Flags: 3 total; -Xincgc -Xmx2048M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 13, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modulargun{v1.32.0a} [Modular Guns Mod] (bin) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 382.05' Renderer: 'GeForce GTX 1070/PCIe/SSE2'
	Launched Version: 1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 1070/PCIe/SSE2 GL version 4.5.0 NVIDIA 382.05, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)