---- Minecraft Crash Report ----
// Don't be sad. I'll do better next time, I promise!

Time: 7/1/17 3:22 PM
Description: Unexpected error

java.lang.NullPointerException: Unexpected error
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:64)
	at net.minecraftforge.client.ForgeHooksClient.renderEquippedItem(ForgeHooksClient.java:232)
	at net.minecraft.client.renderer.ItemRenderer.renderItem(ItemRenderer.java:86)
	at net.minecraft.client.renderer.ItemRenderer.renderItemInFirstPerson(ItemRenderer.java:511)
	at net.minecraft.client.renderer.EntityRenderer.renderHand(EntityRenderer.java:797)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1437)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1091)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1067)
	at net.minecraft.client.Minecraft.run(Minecraft.java:962)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.lederpykewlaid.modulargun.render.RenderGun.renderItem(RenderGun.java:64)
	at net.minecraftforge.client.ForgeHooksClient.renderEquippedItem(ForgeHooksClient.java:232)
	at net.minecraft.client.renderer.ItemRenderer.renderItem(ItemRenderer.java:86)
	at net.minecraft.client.renderer.ItemRenderer.renderItemInFirstPerson(ItemRenderer.java:511)
	at net.minecraft.client.renderer.EntityRenderer.renderHand(EntityRenderer.java:797)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1437)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Player655'/195, l='MpServer', x=-12.87, y=86.99, z=184.26]]
	Chunk stats: MultiplayerChunkCache: 60, 60
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (12,64,192), Chunk: (at 12,4,0 in 0,12; contains blocks 0,0,192 to 15,255,207), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 12942 game time, 12942 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 100 total; [EntityZombie['Zombie'/45, l='MpServer', x=-63.72, y=20.00, z=156.78], EntityBat['Bat'/46, l='MpServer', x=-63.64, y=20.10, z=156.14], EntitySquid['Squid'/47, l='MpServer', x=-49.06, y=46.43, z=147.58], EntityItem['item.item.dyePowder.black'/48, l='MpServer', x=-63.66, y=39.13, z=187.41], EntityXPOrb['Experience Orb'/49, l='MpServer', x=-59.69, y=38.64, z=187.01], EntityXPOrb['Experience Orb'/50, l='MpServer', x=-64.02, y=38.46, z=184.21], EntitySquid['Squid'/51, l='MpServer', x=-55.63, y=42.31, z=200.51], EntitySquid['Squid'/52, l='MpServer', x=-52.21, y=47.53, z=204.45], EntitySquid['Squid'/53, l='MpServer', x=-62.81, y=49.31, z=216.97], EntityItem['item.item.dyePowder.black'/54, l='MpServer', x=-61.91, y=43.13, z=234.47], EntityXPOrb['Experience Orb'/55, l='MpServer', x=-58.16, y=42.45, z=236.72], EntityXPOrb['Experience Orb'/56, l='MpServer', x=-57.41, y=43.55, z=232.11], EntitySquid['Squid'/61, l='MpServer', x=-33.34, y=44.97, z=152.63], EntitySquid['Squid'/62, l='MpServer', x=-42.66, y=39.44, z=185.19], EntitySquid['Squid'/63, l='MpServer', x=-32.47, y=38.46, z=181.55], EntitySquid['Squid'/68, l='MpServer', x=-28.63, y=40.28, z=161.53], EntitySkeleton['Skeleton'/69, l='MpServer', x=-19.50, y=36.00, z=189.09], EntitySheep['Sheep'/70, l='MpServer', x=-21.63, y=63.00, z=188.13], EntityCreeper['Creeper'/74, l='MpServer', x=-7.31, y=18.00, z=145.50], EntityBat['Bat'/75, l='MpServer', x=-3.59, y=17.00, z=154.66], EntitySquid['Squid'/76, l='MpServer', x=-9.86, y=41.80, z=153.57], EntityBat['Bat'/77, l='MpServer', x=-2.04, y=19.00, z=163.25], EntitySpider['Spider'/78, l='MpServer', x=-0.50, y=34.00, z=170.09], EntityZombie['Zombie'/79, l='MpServer', x=-5.56, y=13.00, z=205.44], EntityCreeper['Creeper'/80, l='MpServer', x=-3.88, y=13.00, z=205.78], EntitySkeleton['Skeleton'/81, l='MpServer', x=-8.50, y=31.00, z=205.06], EntityEnderman['Enderman'/82, l='MpServer', x=-10.97, y=42.00, z=196.50], EntityItem['item.item.chickenRaw'/83, l='MpServer', x=-5.53, y=77.13, z=195.22], EntityXPOrb['Experience Orb'/84, l='MpServer', x=-4.99, y=77.34, z=195.75], EntityXPOrb['Experience Orb'/85, l='MpServer', x=-6.28, y=77.61, z=197.25], EntitySkeleton['Skeleton'/86, l='MpServer', x=-10.47, y=13.00, z=219.69], EntitySkeleton['Skeleton'/87, l='MpServer', x=-10.47, y=13.00, z=211.88], EntitySpider['Spider'/88, l='MpServer', x=-9.88, y=13.00, z=218.47], EntityBat['Bat'/89, l='MpServer', x=-3.29, y=44.00, z=219.84], EntityZombie['Zombie'/90, l='MpServer', x=-1.50, y=48.00, z=212.94], EntityPig['Pig'/91, l='MpServer', x=-7.09, y=67.00, z=219.88], EntityCreeper['Creeper'/92, l='MpServer', x=-12.50, y=26.00, z=231.50], EntityItem['item.item.dyePowder.black'/95, l='MpServer', x=7.72, y=32.13, z=136.84], EntityXPOrb['Experience Orb'/96, l='MpServer', x=8.25, y=32.18, z=138.41], EntityXPOrb['Experience Orb'/97, l='MpServer', x=11.87, y=32.13, z=138.69], EntityZombie['Zombie'/98, l='MpServer', x=7.44, y=22.00, z=148.97], EntityZombie['Zombie'/99, l='MpServer', x=6.00, y=21.00, z=149.31], EntityZombie['Zombie'/100, l='MpServer', x=6.72, y=20.00, z=151.50], EntityCreeper['Creeper'/101, l='MpServer', x=5.44, y=21.00, z=152.44], EntityCreeper['Creeper'/102, l='MpServer', x=0.75, y=34.00, z=171.09], EntityCreeper['Creeper'/103, l='MpServer', x=1.69, y=34.00, z=171.00], EntityXPOrb['Experience Orb'/104, l='MpServer', x=-0.04, y=72.50, z=191.38], EntityXPOrb['Experience Orb'/105, l='MpServer', x=0.28, y=72.47, z=190.30], EntitySquid['Squid'/106, l='MpServer', x=10.09, y=43.54, z=218.24], EntityBat['Bat'/107, l='MpServer', x=0.58, y=45.17, z=219.25], EntityMinecartChest['entity.MinecartChest.name'/108, l='MpServer', x=12.50, y=13.50, z=236.50], EntityBat['Bat'/109, l='MpServer', x=15.25, y=13.10, z=236.13], EntitySquid['Squid'/110, l='MpServer', x=11.15, y=43.06, z=226.98], EntityCreeper['Creeper'/111, l='MpServer', x=3.50, y=15.55, z=245.31], EntityCreeper['Creeper'/112, l='MpServer', x=2.50, y=16.00, z=244.59], EntityCreeper['Creeper'/113, l='MpServer', x=3.50, y=17.00, z=255.69], EntityCreeper['Creeper'/114, l='MpServer', x=10.44, y=18.00, z=240.50], EntityBat['Bat'/115, l='MpServer', x=1.28, y=18.10, z=244.47], EntityZombie['Zombie'/120, l='MpServer', x=23.50, y=12.00, z=137.50], EntityZombie['Zombie'/121, l='MpServer', x=16.97, y=18.00, z=149.50], EntityZombie['Zombie'/122, l='MpServer', x=18.84, y=17.00, z=154.53], EntityItem['item.item.dyePowder.black'/123, l='MpServer', x=29.53, y=40.13, z=169.34], EntityXPOrb['Experience Orb'/124, l='MpServer', x=31.16, y=40.20, z=166.63], EntitySquid['Squid'/125, l='MpServer', x=27.68, y=40.35, z=166.35], EntitySkeleton['Skeleton'/126, l='MpServer', x=27.06, y=14.00, z=191.47], EntitySquid['Squid'/127, l='MpServer', x=31.61, y=46.67, z=216.27], EntityWitch['Witch'/128, l='MpServer', x=23.50, y=14.00, z=233.50], EntitySquid['Squid'/129, l='MpServer', x=30.84, y=40.00, z=237.31], EntityZombie['Zombie'/130, l='MpServer', x=20.91, y=14.00, z=245.28], EntityCreeper['Creeper'/131, l='MpServer', x=17.38, y=15.00, z=252.25], EntityZombie['Zombie'/132, l='MpServer', x=24.00, y=15.00, z=253.44], EntitySkeleton['Skeleton'/133, l='MpServer', x=23.94, y=15.00, z=249.53], EntitySlime['Slime'/134, l='MpServer', x=18.31, y=14.00, z=255.00], EntitySquid['Squid'/141, l='MpServer', x=37.71, y=35.48, z=133.06], EntitySkeleton['Skeleton'/142, l='MpServer', x=33.50, y=24.00, z=152.50], EntityZombie['Zombie'/143, l='MpServer', x=39.00, y=18.00, z=149.44], EntityCreeper['Creeper'/144, l='MpServer', x=34.28, y=15.00, z=173.44], EntityBat['Bat'/145, l='MpServer', x=41.56, y=17.45, z=177.75], EntityBat['Bat'/146, l='MpServer', x=34.57, y=15.00, z=171.48], EntityEnderman['Enderman'/147, l='MpServer', x=42.97, y=19.00, z=181.13], EntitySkeleton['Skeleton'/148, l='MpServer', x=44.00, y=19.00, z=181.31], EntitySquid['Squid'/149, l='MpServer', x=35.56, y=51.38, z=186.67], EntityBat['Bat'/150, l='MpServer', x=34.88, y=14.10, z=203.75], EntitySquid['Squid'/151, l='MpServer', x=44.37, y=42.38, z=220.66], EntitySlime['Slime'/152, l='MpServer', x=38.77, y=18.00, z=238.02], EntitySquid['Squid'/153, l='MpServer', x=44.50, y=39.00, z=237.56], EntityMinecartChest['entity.MinecartChest.name'/154, l='MpServer', x=36.50, y=14.34, z=243.50], EntitySkeleton['Skeleton'/155, l='MpServer', x=45.38, y=20.00, z=241.69], EntitySpider['Spider'/156, l='MpServer', x=37.41, y=19.00, z=244.38], EntitySquid['Squid'/157, l='MpServer', x=39.56, y=41.16, z=247.03], EntitySquid['Squid'/158, l='MpServer', x=46.06, y=49.35, z=246.31], EntityCreeper['Creeper'/163, l='MpServer', x=50.34, y=24.00, z=147.75], EntitySkeleton['Skeleton'/164, l='MpServer', x=48.56, y=21.00, z=151.59], EntityCreeper['Creeper'/165, l='MpServer', x=51.00, y=13.00, z=175.56], EntityBat['Bat'/168, l='MpServer', x=58.70, y=20.00, z=236.42], EntityMinecartChest['entity.MinecartChest.name'/169, l='MpServer', x=48.50, y=14.50, z=245.50], EntitySpider['Spider'/170, l='MpServer', x=50.72, y=14.00, z=245.63], EntityCreeper['Creeper'/171, l='MpServer', x=57.28, y=6.00, z=251.91], EntityCreeper['Creeper'/172, l='MpServer', x=59.50, y=6.00, z=253.50], EntityClientPlayerMP['Player655'/195, l='MpServer', x=-12.87, y=86.99, z=184.26]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:415)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2566)
	at net.minecraft.client.Minecraft.run(Minecraft.java:991)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 779552920 bytes (743 MB) / 1038876672 bytes (990 MB) up to 1038876672 bytes (990 MB)
	JVM Flags: 3 total; -Xincgc -Xmx1024M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modularguns{0.1.01} [Modular Guns Mod] (bin) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 382.05' Renderer: 'GeForce GTX 1070/PCIe/SSE2'
	Launched Version: 1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 1070/PCIe/SSE2 GL version 4.5.0 NVIDIA 382.05, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)