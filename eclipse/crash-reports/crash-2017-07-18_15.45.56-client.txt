---- Minecraft Crash Report ----
// Would you like a cupcake?

Time: 7/18/17 3:45 PM
Description: Rendering item

java.lang.NullPointerException: Rendering item
	at com.loaf.modulargun.render.RenderGun.renderItem(RenderGun.java:57)
	at net.minecraftforge.client.ForgeHooksClient.renderInventoryItem(ForgeHooksClient.java:202)
	at net.minecraft.client.renderer.entity.RenderItem.renderItemAndEffectIntoGUI(RenderItem.java:583)
	at net.minecraft.client.gui.inventory.GuiContainerCreative.func_147051_a(GuiContainerCreative.java:968)
	at net.minecraft.client.gui.inventory.GuiContainerCreative.drawGuiContainerBackgroundLayer(GuiContainerCreative.java:795)
	at net.minecraft.client.gui.inventory.GuiContainer.drawScreen(GuiContainer.java:93)
	at net.minecraft.client.renderer.InventoryEffectRenderer.drawScreen(InventoryEffectRenderer.java:44)
	at net.minecraft.client.gui.inventory.GuiContainerCreative.drawScreen(GuiContainerCreative.java:673)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1137)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1067)
	at net.minecraft.client.Minecraft.run(Minecraft.java:962)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.loaf.modulargun.render.RenderGun.renderItem(RenderGun.java:57)
	at net.minecraftforge.client.ForgeHooksClient.renderInventoryItem(ForgeHooksClient.java:202)

-- Item being rendered --
Details:
	Item Type: com.loaf.modulargun.weapons.ItemGun@44ef4e6d
	Item Aux: 0
	Item NBT: null
	Item Foil: false
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderItem.renderItemAndEffectIntoGUI(RenderItem.java:583)
	at net.minecraft.client.gui.inventory.GuiContainerCreative.func_147051_a(GuiContainerCreative.java:968)
	at net.minecraft.client.gui.inventory.GuiContainerCreative.drawGuiContainerBackgroundLayer(GuiContainerCreative.java:795)
	at net.minecraft.client.gui.inventory.GuiContainer.drawScreen(GuiContainer.java:93)
	at net.minecraft.client.renderer.InventoryEffectRenderer.drawScreen(InventoryEffectRenderer.java:44)
	at net.minecraft.client.gui.inventory.GuiContainerCreative.drawScreen(GuiContainerCreative.java:673)

-- Screen render details --
Details:
	Screen name: net.minecraft.client.gui.inventory.GuiContainerCreative
	Mouse location: Scaled: (299, 10). Absolute: (598, 459)
	Screen size: Scaled: (427, 240). Absolute: (854, 480). Scale factor of 2

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['Player44'/288, l='MpServer', x=-127.58, y=74.36, z=225.05]]
	Chunk stats: MultiplayerChunkCache: 220, 220
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (68,64,256), Chunk: (at 4,4,0 in 4,16; contains blocks 64,0,256 to 79,255,271), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 37124 game time, 1162 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 31 total; [EntityBat['Bat'/2, l='MpServer', x=-123.75, y=14.33, z=168.06], EntityBat['Bat'/3, l='MpServer', x=-123.63, y=12.52, z=223.10], EntityBat['Bat'/14, l='MpServer', x=-102.25, y=15.60, z=217.44], EntityChicken['Chicken'/15, l='MpServer', x=-110.56, y=69.00, z=300.56], EntityChicken['Chicken'/16, l='MpServer', x=-99.38, y=67.00, z=301.56], EntityBat['Bat'/18, l='MpServer', x=-94.10, y=44.89, z=193.91], EntityBat['Bat'/19, l='MpServer', x=-80.50, y=30.00, z=216.75], EntityBat['Bat'/20, l='MpServer', x=-92.25, y=31.24, z=217.25], EntityChicken['Chicken'/21, l='MpServer', x=-85.41, y=67.00, z=288.59], EntityBat['Bat'/28, l='MpServer', x=-70.44, y=23.02, z=210.53], EntityBat['Bat'/29, l='MpServer', x=-77.31, y=24.23, z=215.50], EntityChicken['Chicken'/289, l='MpServer', x=-154.69, y=67.00, z=271.53], EntityChicken['Chicken'/290, l='MpServer', x=-165.41, y=68.00, z=270.59], EntityChicken['Chicken'/291, l='MpServer', x=-168.81, y=69.00, z=266.46], EntityBat['Bat'/292, l='MpServer', x=-128.84, y=15.10, z=160.25], EntityChicken['Chicken'/293, l='MpServer', x=-170.38, y=68.00, z=284.31], EntityChicken['Chicken'/39, l='MpServer', x=-61.38, y=64.00, z=184.44], EntityBat['Bat'/40, l='MpServer', x=-62.38, y=20.64, z=195.41], EntityPig['Pig'/297, l='MpServer', x=-195.19, y=67.00, z=185.09], EntityBat['Bat'/41, l='MpServer', x=-65.50, y=22.10, z=206.63], EntityPig['Pig'/298, l='MpServer', x=-199.50, y=67.00, z=188.50], EntityBat['Bat'/42, l='MpServer', x=-54.72, y=39.18, z=218.72], EntityPig['Pig'/299, l='MpServer', x=-199.50, y=67.00, z=188.50], EntityChicken['Chicken'/43, l='MpServer', x=-53.47, y=64.00, z=274.53], EntityPig['Pig'/300, l='MpServer', x=-197.64, y=69.47, z=183.69], EntityChicken['Chicken'/44, l='MpServer', x=-61.44, y=62.62, z=305.84], EntityChicken['Chicken'/301, l='MpServer', x=-173.97, y=68.00, z=151.53], EntityChicken['Chicken'/302, l='MpServer', x=-172.31, y=71.00, z=147.47], EntityChicken['Chicken'/303, l='MpServer', x=-173.09, y=69.00, z=150.16], EntityChicken['Chicken'/304, l='MpServer', x=-174.41, y=69.00, z=150.57], EntityClientPlayerMP['Player44'/288, l='MpServer', x=-127.58, y=74.36, z=225.05]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:415)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2566)
	at net.minecraft.client.Minecraft.run(Minecraft.java:984)
	at net.minecraft.client.main.Main.main(Main.java:164)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_121, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 874868784 bytes (834 MB) / 1038876672 bytes (990 MB) up to 2112618496 bytes (2014 MB)
	JVM Flags: 3 total; -Xincgc -Xmx2048M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 68, allocated: 12, tallocated: 94
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1558 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1558} [Minecraft Forge] (forgeSrc-1.7.10-10.13.4.1558-1.7.10.jar) 
	UCHIJAAAA	modulargun{v1.32.3a} [Modular Guns Mod] (bin) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 382.05' Renderer: 'GeForce GTX 1070/PCIe/SSE2'
	Launched Version: 1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 1070/PCIe/SSE2 GL version 4.5.0 NVIDIA 382.05, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)