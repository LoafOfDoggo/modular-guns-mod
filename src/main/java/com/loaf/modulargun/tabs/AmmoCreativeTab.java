package com.loaf.modulargun.tabs;

import com.loaf.modulargun.main.Handler;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class AmmoCreativeTab extends CreativeTabs {

	public AmmoCreativeTab(int id, String unlocalizedName) 
	{
		super(id, unlocalizedName);
	}

	@Override
	public Item getTabIconItem() 
	{
		return Handler.mag55645;
	}

}
