package com.loaf.modulargun.tabs;

import com.loaf.modulargun.main.Handler;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class GunCreativeTab extends CreativeTabs {

	public GunCreativeTab(int id, String unlocalizedName) 
	{
		super(id, unlocalizedName);
	}

	@Override
	public Item getTabIconItem() 
	{
		return Handler.m4Carbine;
	}

}
