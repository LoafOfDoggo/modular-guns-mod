package com.loaf.modulargun.entity;

import java.util.List;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.server.FMLServerHandler;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.enchantment.EnchantmentProtection;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ReportedException;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.world.World;

public class EntityGrenade extends EntityThrowable {

	public int fuse;
	private Entity exploder;
	private EntityPlayer player;
	private float speed;
	private float damage;
	
	public EntityGrenade(World world, EntityLivingBase entityLiving, float speed, float damage, int fuse) 
	{
		super(world, entityLiving);
		this.speed = speed;
		this.damage = damage;
		this.fuse = fuse;
		this.exploder = entityLiving;
		this.player = (EntityPlayer)entityLiving;
	}
	
	public void onUpdate()
	{
		super.onUpdate();
		if(!this.worldObj.isRemote)
		{
			
		}
		if(this.worldObj.isRemote)
		{
			this.worldObj.spawnParticle("smoke", posX, posY, posZ, motionX, motionY, motionZ);
		}
	}
	
	@Override
	protected void onImpact(MovingObjectPosition movingobjectposition) 
	{
		if (movingobjectposition.entityHit instanceof EntityLivingBase)
		{
			EntityLivingBase entityLiving = ((EntityLivingBase)movingobjectposition.entityHit);
			DamageSource damageSource = DamageSource.causeMobDamage(entityLiving);
			entityLiving.attackEntityFrom(damageSource, damage);
		}
		this.speed = -0.1f;
	}
}
