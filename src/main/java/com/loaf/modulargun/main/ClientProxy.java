package com.loaf.modulargun.main;

import com.loaf.modulargun.fx.EntityFXMuzzleFlash;
import com.loaf.modulargun.render.RenderGun;
import com.loaf.modulargun.render.RenderMag;
import com.loaf.modulargun.server.CommonProxy;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.entity.Entity;
import net.minecraftforge.client.MinecraftForgeClient;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy {
	
	@Override
	public void load() {
		MinecraftForgeClient.registerItemRenderer(Handler.m4Carbine, new RenderGun());
		MinecraftForgeClient.registerItemRenderer(Handler.m16a3, new RenderGun());
		MinecraftForgeClient.registerItemRenderer(Handler.arx160, new RenderGun());
	}
}
