package com.loaf.modulargun.main;

import com.loaf.modulargun.enums.EnumAmmoType;
import com.loaf.modulargun.enums.EnumFPHoldType;
import com.loaf.modulargun.weapons.ItemAmmo;
import com.loaf.modulargun.weapons.ItemGun;
import com.loaf.modulargun.weapons.ItemThrowable;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.item.Item;

public class Handler 
{
	
	//Items
	public static Item mag55645;
	public static Item m4Carbine;
	public static Item m16a3;
	public static Item arx160;
	//public static Item m67;
	
	public static void registerItem(Item item, String name) 
	{
		GameRegistry.registerItem(item, name);
		LanguageRegistry.addName(item, name);
	}
	
	public static void registerItems() 
	{
		//Guns
		m4Carbine = new ItemGun("m4carbine", 31, 3F, 1.2F, 35, 7.5F, EnumFPHoldType.CARBINE, EnumAmmoType.AMMO556X45, 1.25F).setUnlocalizedName("m4Carbine").setTextureName("modulargun:m4carbine");
		registerItem(m4Carbine, "M4 Carbine");
		m16a3 = new ItemGun("m16a3", 31, 3.2F, 1.5F, 35, 7.8F, EnumFPHoldType.ASSAULTRIFLE, EnumAmmoType.AMMO556X45, 1.25F).setUnlocalizedName("m16a3").setTextureName("modulargun:m16a3");
		registerItem(m16a3, "M16A3");
		arx160 = new ItemGun("arx160", 31, 3F, 1.1F, 36, 8.2F, EnumFPHoldType.ASSAULTRIFLE, EnumAmmoType.AMMO556X45, 1.25F).setUnlocalizedName("arx160").setTextureName("modulargun:arx160");
		registerItem(arx160, "ARX-160");
		
		//Grenades
		//m67 = new ItemThrowable(5.0F, 15.0F, 20).setUnlocalizedName("m67").setTextureName("modulargun:m67");
		//registerItem(m67, "M67 Frag");
		
		//Ammo
		mag55645 = new ItemAmmo(4).setUnlocalizedName("ammo556x45").setTextureName("modulargun:ammo556x45");
		registerItem(mag55645, "5.56x45MM NATO");
	}
}
