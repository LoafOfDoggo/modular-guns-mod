package com.loaf.modulargun.main;

import com.loaf.modulargun.server.CommonProxy;
import com.loaf.modulargun.tabs.AmmoCreativeTab;
import com.loaf.modulargun.tabs.GunCreativeTab;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.creativetab.CreativeTabs;

@Mod(modid = "modulargun")
public class ModularGunMain {
	
	public static CreativeTabs tabGuns = new GunCreativeTab(12, "tabGuns");
	public static CreativeTabs tabAmmo = new AmmoCreativeTab(13, "tabAmmo");
	
	@SidedProxy(clientSide="com.loaf.modulargun.main.ClientProxy", serverSide="com.loaf.modulargun.server.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		Handler.registerItems();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e) 
	{
		proxy.load();
	}
}
