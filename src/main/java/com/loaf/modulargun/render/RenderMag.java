package com.loaf.modulargun.render;

import org.lwjgl.opengl.GL11;

import com.loaf.modulargun.weapons.ItemAmmo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;

public class RenderMag implements IItemRenderer {
	
	private ItemRenderer renderer;

	public RenderMag() 
	{
		renderer = new ItemRenderer(Minecraft.getMinecraft());
	}

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) 
	{
		return false;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) 
	{
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data)
	{
		ItemAmmo ammo = (ItemAmmo)item.getItem();
		
		GL11.glPushMatrix();
		
		float scale = 1F;
		GL11.glScalef(scale, scale, scale);
		        	
		GL11.glTranslatef(-0.0F, -0.35F, -0.3F);
		GL11.glRotatef(26.0F, 0.0F, 0.0F, 1.0F);
		GL11.glRotatef(-6.75F, 0.0F, 1.0F, 0.0F);
		        
		IIcon icon = item.getItem().getIcon(item, 0);
		renderer.renderItemIn2D(Tessellator.instance, icon.getMaxU(), icon.getMinV(), icon.getMinU(), icon.getMaxV(), 16, 16, 0.0625F);
		
		GL11.glPopMatrix();
	}

}
