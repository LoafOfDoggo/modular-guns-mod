package com.loaf.modulargun.enums;

public enum EnumAmmoType 
{	
	AMMO9MM, AMMO45ACP, AMMO357SIG, AMMO357MAGNUM,  AMMO556X39, AMMO556X45, AMMO762X39, AMMO762X51;
}
