package com.loaf.modulargun.enums;

public enum EnumAttachmentSlot 
{
	UNDERBARREL, SIDERAIL, BARREL, SIGHT, STOCK, SLING;
}
