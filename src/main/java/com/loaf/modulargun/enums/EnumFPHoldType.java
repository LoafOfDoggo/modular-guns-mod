package com.loaf.modulargun.enums;

public enum EnumFPHoldType {
	
	ASSAULTRIFLE, CARBINE, SNIPER, PISTOL, REVOLVER, SHOTGUN, PDW;

}
