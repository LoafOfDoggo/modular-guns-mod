package com.loaf.modulargun.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;

@SideOnly(Side.CLIENT)
public class GuiButtonModularGun extends GuiButton
{
	public GuiButtonModularGun(int id, int xpos, int ypos, int width, int height, String textToDisplay) 
	{
		super(id, xpos, ypos, width, height, textToDisplay);
	}
}