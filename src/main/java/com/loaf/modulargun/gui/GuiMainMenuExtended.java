package com.loaf.modulargun.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.resources.I18n;

public class GuiMainMenuExtended extends GuiMainMenu
{
	
	public GuiMainMenuExtended()
	{
		super.initGui();
	}
	
    public void initGui()
    {
        int i = this.height / 4 + 48;
    	
        this.buttonList.add(new GuiButtonModularGun(15, this.width / 2 - 100, i + 72 + 12, 98, 20, "Customize Gun"));
    }
    
    protected void actionPerformed(GuiButton p_146284_1_)
    {
        if (p_146284_1_.id == 15)
        {
            this.mc.displayGuiScreen(new GuiCustomize(this));
        }
    }
}
