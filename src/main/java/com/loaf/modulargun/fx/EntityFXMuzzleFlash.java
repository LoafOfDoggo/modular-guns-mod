package com.loaf.modulargun.fx;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class EntityFXMuzzleFlash extends EntityFX
{
	
	private static final ResourceLocation texture = new ResourceLocation("modularguns", "textures/particle/muzzleflash1.png");

    public EntityFXMuzzleFlash(World world, double posX, double posY, double posZ)
    {
        super(world, posX, posY, posZ);
        this.setMaxAge(1);
        this.setGravity(0.0F);
        this.setScale(0.005F);
        noClip = false;
    }

	public void renderParticle(Tessellator tessellator, float partialTicks, float f1, float f2, float f3, float f4, float f5)
    {
        super.renderParticle(tessellator, partialTicks, f1, f2, f3, f4, f5);
        Minecraft.getMinecraft().renderEngine.bindTexture(texture);
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glAlphaFunc(GL11.GL_GREATER, 0.01F);
        tessellator.startDrawingQuads();
        tessellator.setBrightness(getBrightnessForRender(partialTicks));
        float scale = particleScale;
        float x = (float)(prevPosX + (posX - prevPosX) * partialTicks - interpPosX);
        float y = (float)(prevPosY + (posY - prevPosY) * partialTicks - interpPosY);
        float z = (float)(prevPosZ + (posZ - prevPosZ) * partialTicks - interpPosZ);
        tessellator.addVertexWithUV(x - f1 * scale - f4 * scale, y - f2 * scale, z - f3 * scale - f5 * scale, 0, 0);
        tessellator.addVertexWithUV(x - f1 * scale + f4 * scale, y + f2 * scale, z - f3 * scale + f5 * scale, 1, 0);
        tessellator.addVertexWithUV(x + f1 * scale + f4 * scale, y + f2 * scale, z + f3 * scale + f5 * scale, 1, 1);
        tessellator.addVertexWithUV(x + f1 * scale - f4 * scale, y - f2 * scale, z + f3 * scale - f5 * scale, 0, 1);
        tessellator.draw();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDepthMask(true);
        GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
    }
    
	@Override
	public int getFXLayer() 
	{
		return 3;
	}
	
	public EntityFXMuzzleFlash setMaxAge(int maxAge) 
	{
		this.particleMaxAge = maxAge;
		return this;
	}
	
	public EntityFXMuzzleFlash setGravity(float gravity) 
	{
		this.particleGravity = gravity;
		return this;
	}
	
	public EntityFXMuzzleFlash setScale(float scale) 
	{
		this.particleScale = scale;
		return this;
	}
}
