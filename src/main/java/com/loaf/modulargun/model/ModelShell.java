package com.loaf.modulargun.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelShell extends ModelBase {
	
	public ModelRenderer shell;
	
	public ModelShell () 
	{
		int textureX = 8;
		int textureY = 2;
		shell = new ModelRenderer(this, textureX, textureY);
		shell.addBox(0.0F, 0.0F, 0.0F, 3, 1, 1, 0.7F);
		shell.setRotationPoint(0.0F, 0.0F, 0.0F);
		
	}
	
	@Override
	public void render(Entity entity, float f1, float f2, float f3, float f4, float f5, float f6) 
	{
		shell.render(f6);
	}
	
    public void setRotateAngle(ModelRenderer renderer, float x, float y, float z) 
    {
        renderer.rotateAngleX = x;
        renderer.rotateAngleY = y;
        renderer.rotateAngleZ = z;
    }
}
