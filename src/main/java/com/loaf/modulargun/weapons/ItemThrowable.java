package com.loaf.modulargun.weapons;

import org.lwjgl.input.Mouse;

import com.loaf.modulargun.entity.EntityGrenade;
import com.loaf.modulargun.main.ModularGunMain;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemThrowable extends Item {
	
	public int throwLength;
	public float speed;
	public float damage;
	public int fuse;
	private static boolean isAttackButtonDown;
	public int attackButtonId = 0;
	
	public ItemThrowable(float d, float s, int f) 
	{
		this.damage = d;
		this.speed = s;
		this.fuse = f;
		this.setCreativeTab(ModularGunMain.tabAmmo);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int i, boolean isSelected)
	{
		if(entity instanceof EntityPlayer && ((EntityPlayer)entity).inventory.getCurrentItem() == stack) 
		{
			EntityPlayer player = (EntityPlayer)entity;
			isAttackButtonDown = Mouse.isButtonDown(attackButtonId);
			
			if(isAttackButtonDown) 
			{
				world.spawnEntityInWorld(new EntityGrenade(world, player, speed, damage, fuse));
			}
		}
	}
}
