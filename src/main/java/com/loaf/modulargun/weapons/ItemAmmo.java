package com.loaf.modulargun.weapons;

import com.loaf.modulargun.main.ModularGunMain;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemAmmo extends Item 
{
	public int maxMagazineCount;
	
	public ItemAmmo (int maxMagazineCount) 
	{
		this.maxMagazineCount = maxMagazineCount;
		this.setCreativeTab(ModularGunMain.tabAmmo);
		this.setMaxStackSize(maxMagazineCount);
	}
}
